<?php get_header(); ?>

<?php if ( have_posts() ) :?>

	<?php while ( have_posts() ) : the_post(); ?>



	<section id="homepage" class="snap">
		<div class="container-alt">
			<div class="background-slider-fade"></div>
			<div class="background-slider mobile-hide">

				<?php if( have_rows('background_fader_images') ):
					while( have_rows('background_fader_images') ): the_row(); ?>

						<?php $image = get_sub_field('background_fader_image'); ?>
				    	<div class="slide"><img src="<?php echo $image ?>"></div>

			  		<?php endwhile;
				endif; ?>

			</div>
			<div class="content">
				<div class="text mobile-show">
					<h1><?php echo get_field('homepage_title') ?></h1>
				</div>
				<a href="" class="video">
					<iframe class="mobile-hide" frameborder="0" title="<?php _e('Our Tomorrow', 'hattaway'); ?>" type="text/html" height="350" width="100%" src="http://www.youtube.com/embed/<?php echo get_field('our_tomorrow_video_id', 'option');?>?showinfo=0" allowfullscreen></iframe>
					<iframe class="mobile-show" frameborder="0" title="<?php _e('Our Tomorrow', 'hattaway'); ?>" type="text/html" height="200" width="100%" src="http://www.youtube.com/embed/<?php echo get_field('our_tomorrow_video_id', 'option');?>?showinfo=0" allowfullscreen></iframe>
				</a>
				<div class="text mobile-hide">
					<h1><?php echo get_field('homepage_title') ?></h1>
					<h2><?php echo get_field('homepage_text') ?></h2>
					<a href="" class="button-orange plus open-form-overlay"><?php _e('Share Your Voice', 'hattaway'); ?></a>
				</div>
			</div>
		</div>
		<div class="bottom-sticky">
			<a href="#our-tomorrow" class="fl-right bottom-arrow"><?php _e('What is our tomorrow?', 'hattaway'); ?></a>
		</div>
	</section>

	<section id="our-tomorrow" class="snap">
		<div class="mobile-hide top-arrow"><a href="#homepage"><?php _e('Back To Top', 'hattaway'); ?></a></div>
		<div class="mobile-title"><?php _e('Our Tomorrow', 'hattaway'); ?>
			<section class="nav-click">
                <img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/header-menuButton.png">
            </section>
		</div>
		<div class="background-image-fade"></div>
		<div class="background-image">
			<img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/img-bottom-half.png">
		</div>

		<div class="scrolling-content-wrap">
			<div class="cycle-prev"></div>
			<div class="cycle-next"></div>
			<div class="fade-left"></div>
			<div class="fade-right"></div>
			<div class="scrolling-content">
				<!-- <div class="item content first">
						<div class="inner">
							<div class="copy">
								<h1><?php _e('Your Voice Matters', 'hattaway'); ?></h1>
								<p><?php _e('Our Tomorrow is a campaign by more than 85 LGBTQ nonprofit organizations and foundations ', 'hattaway'); ?> <span class="bold"><?php _e('to engage our community in a conversation about our future.', 'hattaway'); ?></span> <?php _e('Your ideas will help build a bigger, bolder movement that leaves no one behind.', 'hattaway'); ?></p>
								<a href="" class="button-orange open-form-overlay"><?php _e('Join the Conversation', 'hattaway'); ?></a>
							</div>
						</div>
				</div> -->

					<?php
					$types = array(
						'hope',
						'fear',
						'idea',
					);
					$results = array();
					foreach ($types as $type) {
						$query = new WP_Query(array(
							'post_type' => 'post',
							'posts_per_page' => 1,
							'meta_query' => array(
								'relation' => 'OR',
								array(
									'key' => "$type-tag",
									'value' => '',
									'compare' => '!=',
								),
								array(
									'key' => "$type-text",
									'value' => '',
									'compare' => '!=',
								),
							),
							'orderby' => 'date',
							'order' => 'DESC',
						));

						$thePost = isset($query->posts[0]) ? $query->posts[0] : array();
						if (!empty($thePost)) {
							$thePost->item_type = $type;
							$results[] = $thePost;
						}
					}

					if (sizeof($results)) {
						$result = current($results);
						$i = 1;
						do {
							if ($i % 3 === 0) {
								// show the share block
								?>
								<div class="item">
									<a href="" class="inner share-your-own open-form-overlay">
										<div class="image"></div>
										<span><?php _e('Share Your Voice', 'hattaway'); ?></span>
									</a>
								</div>
								<?php
								$i++;
								continue;
							}

							$storyRelates = get_story_relates(array($result->ID));
							$result->relates = isset($storyRelates[$result->ID][$result->item_type]) ? $storyRelates[$result->ID][$result->item_type] : 0;

							$post_tags = wp_get_post_tags($result->ID);
							$tags = array();
							foreach ($post_tags as $post_tag) {
								$tags[$post_tag->term_id] = $post_tag->slug;
							}

							$result->similars = get_story_similars($tags);
							$result->image = wp_get_attachment_image_src( get_post_thumbnail_id($result->ID), 'story_wall')[0];;

							// show the item block
							?>
							<div class="item <?php echo $result->item_type; ?>">
								<div class="inner" style="background-image: url('<?php echo  $result->image; ?>');">
									<div class="color-overlay"></div>
									<div class="copy">
										<h2 class="topic"><a href="/story-wall/#/<?php echo $result->item_type; ?>/<?php echo $result->ID; ?>"><span><?php echo $result->item_type; ?></span>
											<?php if (!($title = get_field("{$result->item_type}-tag", $result->ID))) {
												$title = substr(strip_tags(get_field("{$result->item_type}-text")), 0, 40);
											}
											echo $title;
											?>
										</a></h2>
										<p>
										<?php
										$name = get_field('name', $result->item_type);
										$city = get_field('city', $result->item_type);
										$state = get_field('state', $result->item_type);
										if ($name) {
											echo "- $name, $city, $state";
										} else {
											echo "- ";
											_e('Anonymous', 'hattaway');
										}
										?>
										</p>
										<div class="line"></div>
										<h3><span class="relates" id="story<?php echo $result->ID ?>relates"><?php echo $result->relates; ?></span> <span class="people-person"><?php echo $result->relates == 1 ? __('person', 'hattaway') : __('people', 'hattaway'); ?></span> <span><?php _e('can relate', 'hattaway'); ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;<span class="shares" id="story<?php echo $result->ID; ?>shares">0</span> <span><?php _e('people shared', 'hattaway'); ?></span><br/><?php echo $result->similars; ?> <span><?php _e('people have similar hopes', 'hattaway'); ?></span></h3>
									</div>
									<div class="links">
										<a href="" class="relate" data-id="<?php echo $result->ID; ?>" data-topic="<?php echo $result->item_type; ?>"><?php _e('I can relate', 'hattaway'); ?></a>
										<a href="" class="share" data-id="<?php echo $result->ID; ?>" data-url="<?php echo get_the_permalink(10); /* 10 is the story-wall page id */ ?>#<?php echo $result->item_type; ?>/<?php echo $result->ID; ?>" data-title="<?php echo get_field($result->item_type . '-tag', $result->ID) ?: get_field($result->item_type . '-text', $result->ID); ?>"><?php _e('Share', 'hattaway'); ?></a>
									</div>
								</div>
							</div>
							<?php
							$result = next($results);
							$i++;
						} while ($i <= sizeof($results) + 1);
					}

					?>
				</div>
			</div>

		<!-- <div id="horiz_container_outer" class="scrolling-content-wrap mobile-show">
			<div id="horiz_container_inner" class="scrolling-content">
				<div class="horiz_container">

					<div class="item content first">
						<div class="inner">
							<div class="copy">
								<h1><?php _e('Your Voice Matters:', 'hattaway'); ?></h1>
								<p><?php _e('Our Tomorrow is a campaign by more than 70 LGBTQ nonprofit organizations and foundations ', 'hattaway'); ?> <span class="bold"><?php _e('to engage our community in a conversation about our future.', 'hattaway'); ?></span> <?php _e('Your ideas will help build a bigger, bolder movement that leaves no one behind.', 'hattaway'); ?></p>
								<a href="" class="button-orange open-form-overlay"><?php _e('Join the Conversation', 'hattaway'); ?></a>
							</div>
						</div>
					</div>

					<div class="item hope">
						<div class="inner" style="background-image: url('<?php echo get_bloginfo('template_url'); ?>/assets/images/our-tomorrow-block-back1.jpg');">
							<div class="color-overlay"></div>
							<div class="copy">
								<h2><a href=""><span><?php _e('I hope', 'hattaway'); ?> </span><?php _e('that we will soon be able to enjoy the healthcare options we deserve.', 'hattaway'); ?></a></h2>
								<p>-Mark, Washington, D.C.</p>
								<div class="line"></div>
								<h3>201 <span>people can relate</span>&nbsp;&nbsp;|&nbsp;&nbsp;21 <span>people shared</span><br/>22 <span>people have similar hopes</span></h3>
								</h3>
							</div>
							<div class="links">
								<a href="" class="relate"><?php _e('I can relate', 'hattaway'); ?></a>
								<a href="" class="share"><?php _e('Share', 'hattaway'); ?></a>
							</div>
						</div>
					</div>

					<div class="item">
						<a href="" class="inner share-your-own open-form-overlay">
							<div class="image"></div>
							<span><?php _e('Share Your Voice', 'hattaway'); ?></span>
						</a>
					</div>

					<div class="item fear">
						<div class="inner" style="background-image: url('<?php echo get_bloginfo('template_url'); ?>/assets/images/our-tomorrow-block-back2.jpg');">
							<div class="color-overlay"></div>
							<div class="copy">
								<h2><a href=""><span>I fear </span>that lorem ipsum dolar sit lorem ipsum dolar set lorem ipsum dolar.</a></h2>
								<p>-Mark, Washington, D.C.</p>
								<div class="line"></div>
								<h3>201 <span>people can relate</span>&nbsp;&nbsp;|&nbsp;&nbsp;21 <span>people shared</span><br/>22 <span>people have similar hopes</span></h3>
								</h3>
							</div>
							<div class="links">
								<a href="" class="relate"><?php _e('I can relate', 'hattaway'); ?></a>
								<a href="" class="share"><?php _e('Share', 'hattaway'); ?></a>
							</div>
						</div>
					</div>

					<div class="item idea last">
						<div class="inner" style="background-image: url('<?php echo get_bloginfo('template_url'); ?>/assets/images/our-tomorrow-block-back2.jpg');">
							<div class="color-overlay"></div>
							<div class="copy">
								<h2><a href=""><span>My idea is to </span>be able to guarantee equal pay for LGBT employees.</a></h2>
								<p>-Anonymous</p>
								<div class="line"></div>
								<h3>201 <span>people can relate</span>&nbsp;&nbsp;|&nbsp;&nbsp;31 <span>people shared</span><br/>12 <span>people have similar hopes</span></h3>
								</h3>
							</div>
							<div class="links">
								<a href="" class="relate"><?php _e('I can relate', 'hattaway'); ?></a>
								<a href="" class="share"><?php _e('Share', 'hattaway'); ?></a>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div id="scrollbar">
				<div id="track">
			     	<div id="dragBar"></div>
				</div>
			</div>
		</div> -->

	</section>

	<!-- <section class="story-overlay-alt overlay">
        <a class="close" href=""><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/CloseBtn_Small.png"></a>

        <div class="story-inner hope" style="background-image: url('<?php echo get_bloginfo('template_url'); ?>/assets/images/our-tomorrow-block-back1.jpg');">
            <div class="color-overlay"></div>
            <div class="copy">
                <h2><span>My idea is to </span>be able to guarantee equal pay for LGBT employees. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit.</h2>
                <p>-Anonymous</p>
                <div class="line"></div>
                <h3>201 <span>people can relate</span>&nbsp;&nbsp;|&nbsp;&nbsp;31 <span>people shared</span><br/>12 <span>people have similar ideas</span></h3>
                </h3>
            </div>
            <div class="links">
                <a href="" class="relate"><?php _e('I can relate', 'hattaway'); ?></a>
                <a href="" class="share"><?php _e('Share', 'hattaway'); ?></a>
            </div>
        </div>

    </section> -->

	<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>

<?php get_footer(); ?>
