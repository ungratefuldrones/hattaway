	<section class="share-overlay overlay">
        <a class="close" href=""><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/CloseBtn_Small.png"></a>
        <div class="share-inner">
            <div class="title"><?php _e('Share', 'hattaway'); ?></div>
            <?php if ( is_front_page() ) { ?>
                <p><?php _e('You’ve joined the conversation – now help your friends do the same!', 'hattaway'); ?></p>
            <?php } else { ?>
                <p><?php _e('Inspired by this post? Share it with your friends!', 'hattaway'); ?></p>
            <?php } ?>
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <div class="addthis_sharing_toolbox"></div>
        </div>
    </section>

    <section class="nav-overlay overlay">
        <a class="close" href=""><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/CloseBtn_Small.png"></a>
        <nav>
            <?php printMenu ('Main Menu Top', '', ''); ?>
            <div class="bottom">
                <?php printMenu ('Main Menu Bottom', '', ''); ?>
            </div>
            <div class="mobile-social">
                <section class="social">
                    <a target="_blank" href="<?php echo get_field('facebook', 'option');?>"><img class="facebook" src="<?php echo get_bloginfo('template_url'); ?>/assets/images/header-facebook.png"></a>
                    <a target="_blank" href="<?php echo get_field('twitter', 'option');?>"><img class="twitter" src="<?php echo get_bloginfo('template_url'); ?>/assets/images/header-twitter.png"></a>
                    <a target="_blank" href="<?php echo get_field('youtube', 'option');?>"><img class="youtube" src="<?php echo get_bloginfo('template_url'); ?>/assets/images/header-youtube.png"></a>
                    <a target="_blank" href="<?php echo get_field('instagram', 'option');?>"><img class="instagram" src="<?php echo get_bloginfo('template_url'); ?>/assets/images/header-instagram.png"></a>
                </section>
                <section class="share-btn">
                    <img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/header-shareBtn.png">
                </section>
            </div>
        </nav>
    </section>

    <section class="home-video-overlay overlay">
        <div class="home-video-inner container-alt">
            <a class="close" href=""><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/CloseBtn_Small.png"></a>
            <div class="iframe-wrap">
                <!-- <iframe id="player" width="1200" height="600" src="http://www.youtube.com/embed/ofG61k3NLF8?rel=0&wmode=Opaque&enablejsapi=1" frameborder="0"></iframe> -->
                <iframe id="player" width="1200" height="600" src="http://www.youtube.com/embed/<?php echo get_field('our_tomorrow_video_id', 'option');?>?rel=0&wmode=Opaque&enablejsapi=1" frameborder="0"></iframe>
            </div>
        </div>
    </section>

    <section class="form-overlay first">
        <a href="" class="back"></a>
        <div class="close-count">
            <div class="count">
                <span>1</span>/7
            </div>
            <a class="close-form" href=""><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/CloseBtn_Small.png"></a>
        </div>
        <section class="error"></section>
        <section class="title-wrap">
            <a href="" class="main" data-type="hope"><?php _e('Hope', 'hattaway'); ?></a>
            <a href="" data-type="fear"><?php _e('Fear', 'hattaway'); ?></a>
            <a href="" data-type="idea"><?php _e('Idea', 'hattaway'); ?></a>
        </section>
        <form id="form-voice" name="form-voice" method="post" action="/wp-admin/admin-ajax.php" enctype="multipart/form-data">
            <section class="button">
                <button class="regular"><span class="text"></span><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/WhiteDownArrow@2x.png"></button>
                <button class="special" type="submit"><span class="text"><?php _e('Submit Your Post(s)', 'hattaway'); ?></span></button>
            </section>
            <section class="wrap">

                <section class="inner-wrap intro">
                    <div class="content-wrap">
                        <h1><?php _e('Share Your Voice', 'hattaway'); ?></h1>
                        <p class="extra-large"><?php _e('On the following screens, you can share your hopes and fears for the future—and your ideas for making tomorrow better for all of us.', 'hattaway'); ?></p>
                        <p class="large"><?php _e('You may choose to remain anonymous.', 'hattaway'); ?></p>
                    </div>
                </section>

                <section class="inner-wrap name-location">
                    <div class="content-wrap">
                        <input type="hidden" name="fromlang" id="fromlang" value="<?php echo ICL_LANGUAGE_CODE; ?>">
                        <input class="mandatory" type="text" name="name" value="" placeholder="<?php _e('First Name', 'hattaway'); ?>">
                        <input class="mandatory" type="text" name="city" value="" placeholder="<?php _e('City', 'hattaway'); ?>">
                        <select name="states" id="states">
                            <option value="" selected><?php _e('State', 'hattaway'); ?></option>
                            <?php
                            foreach (json_decode(file_get_contents(get_template_directory() . '/assets/js/states.json')) as $stateAbbrev => $stateName) {
                                echo '<option value="' . $stateAbbrev . '">' . $stateName . '</option>';
                            }
                            ?>
                        </select>
                        <input type="file" name="file" id="file" class="photo-upload">
                        <div class="fake-photo-upload"><?php _e('Upload Photo', 'hattaway'); ?> <img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/icon-CameraIcon@2x.png"></div>
                        <div class="fake-photo-upload-name"><p></p></div>
                        <div class="anonymous">
                            <p><input type="checkbox" name="anonymous" value=""><?php _e('I choose to remain anonymous', 'hattaway'); ?></p>
                        </div>
                    </div>
                </section>

                <section class="inner-wrap topic hope" data-type="hope">
                    <div class="content-wrap">
                        <div class="topic-first">
                            <div><?php _e('My Hope', 'hattaway'); ?>: <span>(<?php _e('give your post a title', 'hattaway'); ?>)</span></div>
                            <input type="text" name="hope-tag" value="" maxlength="40" placeholder="<?php _e('Give Your Post a Title', 'hattaway'); ?>">
                            <div class="counter">40</div>
                        </div>
                        <div class="topic-second">
                            <textarea rows="6" name="hope-text" value="" placeholder="<?php _e('My hope for our tomorrow is...', 'hattaway'); ?>"></textarea>
                        </div>
                        <input class="video-upload" type="text" name="hope-video" placeholder="<?php _e('Enter your YouTube link here', 'hattaway'); ?>">
                        <div class="fake-video-upload"><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/icon-VideoCameraIcon.png"> <?php _e('Want to include a video? (optional)', 'hattaway'); ?></div>
                    </div>
                </section>

                <section class="inner-wrap topic fear" data-type="fear">
                    <div class="content-wrap">
                        <div class="topic-first">
                            <div><?php _e('My Fear', 'hattaway'); ?>: <span>(<?php _e('give your post a title', 'hattaway'); ?>)</span></div>
                            <input type="text" name="fear-tag" value="" maxlength="40" placeholder="<?php _e('Give Your Post a Title', 'hattaway'); ?>">
                            <div class="counter">40</div>
                        </div>
                        <div class="topic-second">
                            <textarea rows="6" name="fear-text" value="" placeholder="<?php _e('I worry that...', 'hattaway'); ?>"></textarea>
                        </div>
                        <input class="video-upload" type="text" name="fear-video" placeholder="<?php _e('Enter your YouTube link here', 'hattaway'); ?>">
                        <div class="fake-video-upload"><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/icon-VideoCameraIcon.png"> <?php _e('Want to include a video? (optional)', 'hattaway'); ?></div>
                    </div>
                </section>

                <section class="inner-wrap topic idea" data-type="idea">
                    <div class="content-wrap">
                        <div class="topic-first">
                            <div><?php _e('My Idea', 'hattaway'); ?>: <span>(<?php _e('give your post a title', 'hattaway'); ?>)</span></div>
                            <input type="text" name="idea-tag" value="" maxlength="40" placeholder="<?php _e('Give Your Post a Title', 'hattaway'); ?>">
                            <div class="counter">40</div>
                        </div>
                        <div class="topic-second">
                            <textarea rows="6" name="idea-text" value="" placeholder="<?php _e('One thing our movement can do to make our tomorrow brighter is...', 'hattaway'); ?>"></textarea>
                        </div>
                        <input class="video-upload" type="text" name="idea-video" placeholder="<?php _e('Enter your YouTube link here', 'hattaway'); ?>">
                        <div class="fake-video-upload"><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/icon-VideoCameraIcon.png"> <?php _e('Want to include a video? (optional)', 'hattaway'); ?></div>
                    </div>
                </section>

                <section class="inner-wrap confidential-ques">
                    <div class="mobile-top-gradient"></div>
                    <div class="mobile-bottom-gradient"></div>
                    <div class="content-wrap">
                        <div class="columns-wrap">
                            <p><?php _e('Please help us understand the issues that matter most to you and others like you by answering the questions below.', 'hattaway'); ?> <span class="bold"><?php _e('Our Tomorrow will keep your answers completely confidential.', 'hattaway'); ?></span> <?php _e('Your personal information will never be sold or shared — and it will not appear with your post.', 'hattaway'); ?></p>
                            <div class="columns-inner-wrap">
                                <div class="left">
                                    <select name="age" id="age">
                                        <option value="" selected><?php _e('Age', 'hattaway'); ?></option>
                                        <option value="13-17">13-17</option>
                                        <option value="18-24">18-24</option>
                                        <option value="25-34">25-34</option>
                                        <option value="35-44">35-44</option>
                                        <option value="45-54">45-54</option>
                                        <option value="55-64">55-64</option>
                                        <option value="65-74">65-74</option>
                                        <option value="75+">75+</option>
                                    </select>
                                    <div class="select-multiple multiple">
                                        <select name="sexual_orientation" class="has-other" id="sexual_orientation">
                                            <option value="" selected><?php _e('Sexual Orientation', 'hattaway'); ?></option>
                                            <option value="<?php _e('Asexual', 'hattaway'); ?>"><?php _e('Asexual', 'hattaway'); ?></option>
                                            <option value="<?php _e('Bisexual', 'hattaway'); ?>"><?php _e('Bisexual', 'hattaway'); ?></option>
                                            <option value="<?php _e('Gay', 'hattaway'); ?>"><?php _e('Gay', 'hattaway'); ?></option>
                                            <option value="<?php _e('Lesbian', 'hattaway'); ?>"><?php _e('Lesbian', 'hattaway'); ?></option>
                                            <option value="<?php _e('Pansexual', 'hattaway'); ?>"><?php _e('Pansexual', 'hattaway'); ?></option>
                                            <option value="<?php _e('Queer', 'hattaway'); ?>"><?php _e('Queer', 'hattaway'); ?></option>
                                            <option value="<?php _e('Questioning', 'hattaway'); ?>"><?php _e('Questioning', 'hattaway'); ?></option>
                                            <option value="<?php _e('Straight', 'hattaway'); ?>"><?php _e('Straight', 'hattaway'); ?></option>
                                            <option value="<?php _e('Other', 'hattaway'); ?>"><?php _e('Other', 'hattaway'); ?></option>
                                        </select>
                                        <div class="other-hidden">
                                            <div class="input-box">
                                                <p><?php _e('How do you identify yourself?', 'hattaway'); ?></p>
                                                <div>
                                                    <input type="text" name="gender-other" value="">
                                                    <div class="other-submit"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="multiple checks">
                                        <label><?php _e('Gender Identity', 'hattaway'); ?><span>(<?php _e('Select all that apply', 'hattaway'); ?>)</span></label>
                                        <div class="checkboxes" id="gender">
                                            <div>
                                                <input type="checkbox" name="gender[]" value="<?php _e('Agender', 'hattaway'); ?>">
                                                <span><?php _e('Agender', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="gender[]" value="<?php _e('Female', 'hattaway'); ?>">
                                                <span><?php _e('Female', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="gender[]" value="<?php _e('Gender Nonconforming/Genderqueer', 'hattaway'); ?>">
                                                <span><?php _e('Gender Nonconforming/Genderqueer', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="gender[]" value="<?php _e('Intersex', 'hattaway'); ?>">
                                                <span><?php _e('Intersex', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="gender[]" value="<?php _e('Male', 'hattaway'); ?>">
                                                <span><?php _e('Male', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="gender[]" value="<?php _e('Transgender', 'hattaway'); ?>">
                                                <span><?php _e('Transgender', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="gender[]" value="<?php _e('Two-Spirit', 'hattaway'); ?>">
                                                <span><?php _e('Two-Spirit', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input class="has-other" type="checkbox" name="gender[]" value="<?php _e('Other', 'hattaway'); ?>">
                                                <span><?php _e('Other', 'hattaway'); ?></span>
                                            </div>
                                        </div>
                                        <div class="other-hidden">
                                            <div class="input-box">
                                                <p><?php _e('How do you identify yourself?', 'hattaway'); ?></p>
                                                <div>
                                                    <input type="text" name="gender-other" value="">
                                                    <div class="other-submit"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="multiple checks">
                                        <label><?php _e('Race/Ethnicity', 'hattaway'); ?><span>(<?php _e('Select all that apply', 'hattaway'); ?>)</span></label>
                                        <div class="checkboxes" id="race">
                                            <div>
                                                <input type="checkbox" name="race[]" value="<?php _e('American Indian, Native/Indigenous American or Alaska Native Asian', 'hattaway'); ?>">
                                                <span><?php _e('American Indian, Native/Indigenous American or Alaska Native Asian', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="race[]" value="<?php _e('Asian, South Asian or Southeast Asian', 'hattaway'); ?>">
                                                <span><?php _e('Asian, South Asian or Southeast Asian', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="race[]" value="<?php _e('Black or African-American', 'hattaway'); ?>">
                                                <span><?php _e('Black or African-American', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="race[]" value="<?php _e('Latino or Spanish Origins', 'hattaway'); ?>">
                                                <span><?php _e('Latino or Spanish Origins', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="race[]" value="<?php _e('Native Hawaiian or Other Pacific Islander', 'hattaway'); ?>">
                                                <span><?php _e('Native Hawaiian or Other Pacific Islander', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="race[]" value="<?php _e('White', 'hattaway'); ?>">
                                                <span><?php _e('White', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input class="has-other" type="checkbox" name="race[]" value="<?php _e('Other', 'hattaway'); ?>">
                                                <span><?php _e('Other', 'hattaway'); ?></span>
                                            </div>
                                        </div>
                                        <div class="other-hidden">
                                            <div class="input-box">
                                                <p><?php _e('How do you identify yourself?', 'hattaway'); ?></p>
                                                <div>
                                                    <input type="text" name="race-other" value="">
                                                    <div class="other-submit"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="right">
                                    <div class="multiple checks">
                                        <label><?php _e('Relationship Status', 'hattaway'); ?><span>(<?php _e('Select all that apply', 'hattaway'); ?>)</span></label>
                                        <div class="checkboxes" id="relationship">
                                            <div>
                                                <input type="checkbox" name="relationship[]" value="<?php _e('Single', 'hattaway'); ?>">
                                                <span><?php _e('Single', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="relationship[]" value="<?php _e('Married/Civil Union/Domestic Partnership', 'hattaway'); ?>">
                                                <span><?php _e('Married/Civil Union/Domestic Partnership', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="relationship[]" value="<?php _e('Unmarried Partnership', 'hattaway'); ?>">
                                                <span><?php _e('Unmarried Partnership', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="relationship[]" value="<?php _e('Divorced', 'hattaway'); ?>">
                                                <span><?php _e('Divorced', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="relationship[]" value="<?php _e('Widowed', 'hattaway'); ?>">
                                                <span><?php _e('Widowed', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="relationship[]" value="<?php _e('Open Relationship', 'hattaway'); ?>">
                                                <span><?php _e('Open Relationship', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="relationship[]" value="<?php _e('Polyamorous', 'hattaway'); ?>">
                                                <span><?php _e('Polyamorous', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input class="has-other" type="checkbox" name="relationship[]" value="<?php _e('Other', 'hattaway'); ?>">
                                                <span><?php _e('Other', 'hattaway'); ?></span>
                                            </div>
                                        </div>
                                        <div class="other-hidden">
                                            <div class="input-box">
                                                <p><?php _e('How do you identify yourself?', 'hattaway'); ?></p>
                                                <div>
                                                    <input type="text" name="relationship-other" value="">
                                                    <div class="other-submit"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="multiple checks">
                                        <label><?php _e('Parental Status', 'hattaway'); ?><span>(<?php _e('Select all that apply', 'hattaway'); ?>)</span></label>
                                        <div class="checkboxes">
                                            <div>
                                                <input type="checkbox" name="parental" value="<?php _e('No Children', 'hattaway'); ?>">
                                                <span><?php _e('No Children', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="parental" value="<?php _e('One or More Children Under 18', 'hattaway'); ?>">
                                                <span><?php _e('One or More Children Under 18', 'hattaway'); ?></span>
                                            </div>
                                            <div>
                                                <input type="checkbox" name="parental" value="<?php _e('One or More Children Over 18', 'hattaway'); ?>">
                                                <span><?php _e('One or More Children Over 18', 'hattaway'); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <select name="income" id="income">
                                        <option value="" selected><?php _e('Income Level', 'hattaway'); ?></option>
                                        <option value="$0 - $10,000">$0 - $10,000</option>
                                        <option value=$"10,001 - $25,000">$10,001 - $25,000</option>
                                        <option value="$25,001 - $40,000">$25,001 - $40,000</option>
                                        <option value="$40,001 - $55,000">$40,001 - $55,000</option>
                                        <option value="$55,001 - $70,000">$55,001 - $70,000</option>
                                        <option value="$70,001 - $85,000">$70,001 - $85,000</option>
                                        <option value="$85,001 - $100,000">$85,001 - $100,000</option>
                                        <option value="$100,000+">$100,000+</option>
                                    </select>
                                    <input class="zipcode-input" type="text" name="zipcode" value="" placeholder="ZIP Code">
                                    <input type="hidden" name="action" id="action" value="mq_upload_action">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="inner-wrap review">
                    <div class="gradient"></div>
                    <div class="content-wrap">
                        <div class="title"><?php _e('Preview Your Posts', 'hattaway'); ?></div>
                        <div class="wrapper">
                            <div class="column hope" data-type="hope">
                                <div class="item large">
                                    <div class="inner">
                                        <div class="background-image"></div>
                                        <div class="color-overlay"></div>
                                        <div class="copy">
                                            <h2 class="topic"><?php _e('I HOPE', 'hattaway'); ?> <span></span></h2>
                                            <p>- <span class="name"></span><span class="city"></span><span class="state"></span></p>
                                            <div class="line"></div>
                                            <h3>0 <span><?php _e('people can relate', 'hattaway'); ?></span><br/>0 <span><?php _e('people shared', 'hattaway'); ?></span><br/>0 <span><?php _e('people have similar hopes', 'hattaway'); ?></span></h3>
                                        </div>
                                        <div class="links">
                                            <div class="relate"><?php _e('I can relate', 'hattaway'); ?></div>
                                            <div class="share"><?php _e('Share', 'hattaway'); ?></div>
                                        </div>
                                    </div>
                                </div>
                                <a class="edit" href=""><?php _e('Edit', 'hattaway'); ?></a>
                            </div>

                            <div class="column fear" data-type="fear">
                                <div class="item large">
                                    <div class="inner">
                                        <div class="background-image"></div>
                                        <div class="color-overlay"></div>
                                        <div class="copy">
                                            <h2 class="topic"><?php _e("I'M AFRAID", "hattaway"); ?><span></span></h2>
                                            <p>- <span class="name"></span><span class="city"></span><span class="state"></span></p>
                                            <div class="line"></div>
                                            <h3>0 <span><?php _e('people can relate', 'hattaway'); ?></span><br/>0 <span><?php _e('people shared', 'hattaway'); ?></span><br/>0 <span><?php _e('people have similar hopes', 'hattaway'); ?></span></h3>
                                        </div>
                                        <div class="links">
                                            <div class="relate"><?php _e('I can relate', 'hattaway'); ?></div>
                                            <div class="share"><?php _e('Share', 'hattaway'); ?></div>
                                        </div>
                                    </div>
                                </div>
                                <a class="edit" href=""><?php _e('Edit', 'hattaway'); ?></a>
                            </div>

                            <div class="column idea" data-type="idea">
                                <div class="item large">
                                    <div class="inner">
                                        <div class="background-image"></div>
                                        <div class="color-overlay"></div>
                                        <div class="copy">
                                            <h2 class="topic"><?php _e('MY IDEA', 'hattaway'); ?> <span></span></h2>
                                            <p>- <span class="name"></span><span class="city"></span><span class="state"></span></p>
                                            <div class="line"></div>
                                            <h3>0 <span><?php _e('people can relate', 'hattaway'); ?></span><br/>0 <span><?php _e('people shared', 'hattaway'); ?></span><br/>0 <span><?php _e('people have similar hopes', 'hattaway'); ?></span></h3>
                                        </div>
                                        <div class="links">
                                            <div class="relate"><?php _e('I can relate', 'hattaway'); ?></div>
                                            <div class="share"><?php _e('Share', 'hattaway'); ?></div>
                                        </div>
                                    </div>
                                </div>
                                <a class="edit" href=""><?php _e('Edit', 'hattaway'); ?></a>
                            </div>

                        </div>
                    </div>

                </section>

                <section class="inner-wrap thank-you">
                    <div class="content-wrap">
                        <h1><?php _e('Thank You', 'hattaway'); ?></h1>
                        <p class="extra-large"><?php _e('Thanks for sharing your voice! Now, share it with your friends using the button below.', 'hattaway'); ?></p>
                        <div class="email-share">
                            <a class="topic hope share-after-submit" href="" data-url="<?php echo get_the_permalink(10); /* the story wall page is id 10 */ ?>#hope/<?php echo ''; ?>" data-title=""><?php _e('Share Your Hope On Social Media', 'hattaway'); ?> <img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/form-SocialShareIcon.png"></a>
                            <a class="topic fear share-after-submit" href="" data-url="<?php echo get_the_permalink(10); /* the story wall page is id 10 */ ?>#fear/<?php echo ''; ?>" data-title=""><?php _e('Share Your Fear On Social Media', 'hattaway'); ?><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/form-SocialShareIcon.png"></a>
                            <a class="topic idea share-after-submit" href="" data-url="<?php echo get_the_permalink(10); /* the story wall page is id 10 */ ?>#idea/<?php echo ''; ?>" data-title=""><?php _e('Share Your Idea On Social Media', 'hattaway'); ?><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/form-SocialShareIcon.png"></a>
                        </div>
                    </div>
                </section>

            </section>
            <div id="hidefollow" style="display: none;"><?php _e('Follow the Conversation', 'hattaway'); ?></div>
        </form>
    </section>

    <?php wp_footer(); ?>

    <script>
        var currentIpAddress = "<?php
        // Provide the user's current IP address for use in Javascript
        // Otherwise, we would need to use $.ajax. This is just easier and ensures that
        // the data is available when we need it.
        echo $_SERVER['REMOTE_ADDR'];
    ?>";</script>

    <script type="text/javascript" src="http://www.youtube.com/player_api"></script>
    <script src="<?php echo get_bloginfo('template_url'); ?>/assets/dist/hattaway.all.js"></script>

    <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.3.3/underscore-min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.2/backbone-min.js" type="text/javascript"></script>

    <script type='text/javascript' src='<?php echo site_url(); ?>/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
    <script type='text/javascript' src='<?php echo site_url(); ?>/wp-includes/js/jquery/jquery.form.min.js?ver=3.37.0'></script>
    <script type='text/javascript' src='<?php echo get_bloginfo('template_url'); ?>/assets/js/jquery.scrollFix.min.js'></script>

    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <!-- http://www.addthis.com/blog/2013/05/07/a-brief-history-of-using-addthis-dynamically/ -->
    <script type="text/javascript">
        var addthisScript = document.createElement('script');
        addthisScript.setAttribute('src', '//s7.addthis.com/js/300/addthis_widget.js#domready=1&pubid=ra-54d297232e7ef34c');
        document.body.appendChild(addthisScript);
    </script>
    <script type="text/javascript">

        $(document).ready(function() {
             $('#form-voice').ajaxForm({
                data: {'action': 'mq_ajax_upload'}
             });

            $(document).on('click', 'section.button.submit button[type="submit"]', function(e) {
                e.preventDefault();
                var thisBetterWork = {};
                $('input').each(function () {
                   // thisBetterWork[$(this).attr('name')] = $(this).val();
                });
                $('#form-voice').ajaxSubmit(function(response)  {
                    console.log($.parseJSON(response));
                    var jsonResponse = $.parseJSON(response);
                    $('.form-overlay').find('.inner-wrap.review .column.active').each(function() {
                        var $topic = $(this).data('type');
                        $('.email-share').find('.' + $topic).attr('data-title', jsonResponse[$topic + '-tag']);
                        $('.email-share').find('.' + $topic).attr('data-url', jsonResponse['url'] + '#' + $topic + '/' + jsonResponse['id']);
                        //$('.email-share').find('.' + $topic).addClass('active');
                    });
                });
            });

        });
</script>

</body>
</html>
