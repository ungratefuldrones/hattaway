$(function() {

	var $window = $(window),
		$document = $(document),
		$body = $('body'),
		$wHeight = $window.height();

	$window.resize(function() {
		$wHeight = $window.height();
	}).trigger('resize');



	/*-------------------------------------------------*/
	/* =  preloader function
	/*-------------------------------------------------*/
	var winDow = $(window);
	var body = $('body');
	body.addClass('active');

	winDow.load( function(){
		var mainDiv = $('#wrapper'),
			preloader = $('.preloader');

			mainDiv.fadeOut(600, function(){
				mainDiv.delay(400).addClass('preactive');
				//body.delay(500).css('background', '#ffffff');
			});
	});

	/****************************************
		Smooth Scroll Anchor Tags
	****************************************/

	$.localScroll({
		hash: true
	});







	/****************************************
		Overlays
	****************************************/

	$('.close').click(function(e) {
		e.preventDefault();
		var $thisOverlay =  $(this).closest('.overlay');
		$thisOverlay.fadeOut(500);
		$body.removeClass('overflow-hidden');
		// if overlay is video
		if ( $thisOverlay.hasClass('home-video-overlay') ) {
			player.stopVideo();
		}
	});




	/****************************************
		Header
	****************************************/

	/* sharing */

	$('.share-btn, .nav-click').click(function(e) {
		e.preventDefault();
		if ( $(this).hasClass('share-btn') ) {
			$('.share-overlay').fadeIn(500);
			$body.addClass('overflow-hidden');


			$toolbox = $('.share-overlay .addthis_sharing_toolbox');
			$toolbox.attr('data-url', window.location.href);
			$toolbox.attr('data-title', document.getElementsByTagName("title")[0].innerHTML);
			config = $.extend(true, {}, window.addthis_config);

			addthis.update('share', 'url', window.location.href);
			addthis.update('share', 'title', document.getElementsByTagName("title")[0].innerHTML);

		}
		else {
			$('.nav-overlay').fadeIn(500);
		}
		$body.addClass('overflow-hidden');
	});

	/* make homepage logo a block element */

	$('.logo-homepage img').removeClass('hide');





	/****************************************
		Homepage/Our Tomorrow Pages
	****************************************/

	var $homepage = $('#homepage'),
		$ourTomorrow = $('#our-tomorrow');

	/* video click */

	$('.home-video-click').click(function(e) {
		e.preventDefault();
		$('.home-video-overlay').fadeIn(500);
		$body.addClass('overflow-hidden');
		$window.trigger('resize'); // this re-sizes iframe
	});

	/* snap effect */

	if ( $homepage.length > 0 ) {
		$body.panelSnap({
			panelSelector: '.snap',
			slideSpeed: 500,
			directionThreshold: 100,
		});
	}

	/* set height */

	$window.resize(function() {
		$homepage.add($ourTomorrow).height( $wHeight );
	}).trigger('resize');

	/* slider */

	var $backgroundSlider = $('.background-slider');

	// add left class to first slide
	$backgroundSlider.on('cycle-post-initialize', function(event, optionHash) {
	    $backgroundSlider.find('.cycle-slide-active').addClass('left');
	});

	$backgroundSlider.cycle({
		speed: 3000,
		slides: '.slide',
		timeout: 3500,
		log: false
	});

	// before transitioning add class to next slide
	$backgroundSlider.on('cycle-before', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
	    if ( $(outgoingSlideEl).hasClass('left') ) {
	    	$(incomingSlideEl).addClass('right');
				}
				else {
	    	$(incomingSlideEl).addClass('left');
				}
			});

	// after transitioning take classes off of siblings
	$backgroundSlider.on('cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
	    $(outgoingSlideEl).removeClass('left right');
	});

	/* overlay for our tomorrow blocks */

	// $('.item .copy h2 a').click(function(e) {
	// 	e.preventDefault();
	// 	$('.story-overlay-alt').fadeIn(500);
	// 	$body.addClass('overflow-hidden');
	// });

	/* our tomorrow scrollbar */

	// if ( $window.width() >= 768 ) {
	// 	$window.resize(function() {
	// 		horizontalScroll();
	// 	}).trigger('resize');
	// }
	// function horizontalScroll() {
	// 	$('#horiz_container_outer.mobile-hide').horizontalScroll();
	// }

	$('.scrolling-content').cycle({
		fx: 'carousel',
		speed: 500,
		slides: '.slide',
		timeout: 0,
		slides: '.item',
		timeout: 0,
		prev: '.cycle-prev',
		next: '.cycle-next'
	});





	/****************************************
		Story Wall Page
	****************************************/

	// Isotope extension for centered masonry

	$.Isotope.prototype._getCenteredMasonryColumns = function() {
		this.width = this.element.width();
		var parentWidth = this.element.parent().width();
		// i.e. options.masonry && options.masonry.columnWidth
		var colW = this.options.masonry && this.options.masonry.columnWidth ||
			// or use the size of the first item
			this.$filteredAtoms.outerWidth(true) ||
			// if there's no items, use size of container
			parentWidth;
		var cols = Math.floor( parentWidth / colW );
		cols = Math.max( cols, 1 );
		// i.e. this.masonry.cols = ....
		this.masonry.cols = cols;
		// i.e. this.masonry.columnWidth = ...
		this.masonry.columnWidth = colW;
	};

	$.Isotope.prototype._masonryReset = function() {
		// layout-specific props
		this.masonry = {};
		// FIXME shouldn't have to call this again
		this._getCenteredMasonryColumns();
		var i = this.masonry.cols;
		this.masonry.colYs = [];
		while (i--) {
			this.masonry.colYs.push( 0 );
		}
	};

	$.Isotope.prototype._masonryResizeChanged = function() {
		var prevColCount = this.masonry.cols;
		// get updated colCount
		this._getCenteredMasonryColumns();
		return ( this.masonry.cols !== prevColCount );
	};

	$.Isotope.prototype._masonryGetContainerSize = function() {
		var unusedCols = 0,
		i = this.masonry.cols;
		// count unused columns
		while ( --i ) {
			if ( this.masonry.colYs[i] !== 0 ) {
				break;
			}
			unusedCols++;
		}
		return {
			height : Math.max.apply( Math, this.masonry.colYs ),
			// fit container to columns that have been used;
			width : (this.masonry.cols - unusedCols) * this.masonry.columnWidth
		};
	};

	if ( $('.story-wall').length ) {

		var StoryItem = Backbone.Model.extend();

		var Story = Backbone.Collection.extend({
			model: StoryItem
		});

		var storyCollection = new Story(storyitems);

		var StoryView = Backbone.View.extend({
			tagName: 'div',
			className: 'item',
			template: $('#storyItemTemplate').html(),
			outerOverlayTemplate : _.template( $('#storyOverlayTemplate').html() ),
			innerOverlayTemplate : _.template( $('#storyInnerOverlayTemplate').html() ),
			events: {
				'click h2 a' : 'openOverlay'
			},

			openOverlay : function(e) {
				e.preventDefault();

				var $thisTarget = $(e.target),
					$storyOverlay = $('.story-overlay'),
					$urlTopic = $thisTarget.closest('.item').find('.inner').data('topic'),
					$urlId = $thisTarget.closest('.item').find('.inner').data('id'),
					outerOverlay = this.outerOverlayTemplate,
					innerOverlay = this.innerOverlayTemplate(this.model.toJSON());

				window.history.pushState('', '', '#/' + $urlTopic + '/' + $urlId);

				if ( !$thisTarget.closest('.item').hasClass('open') ) {
					$storyOverlay.append(outerOverlay).fadeIn(500);
					$thisTarget.closest('.item').addClass('open');
					$body.addClass('overflow-hidden position-fixed');
				}

				$storyOverlay.append(innerOverlay).fadeIn(500);

				var $storyOverlayInner = $storyOverlay.find('.story-inner'),
					$topic = $storyOverlayInner.data('topic');

				$storyOverlayInner.addClass($topic).find('.copy h2 span').html( hopeIdeaFear($topic) );
				var sharesNumberSpan = $storyOverlayInner.children('.copy').children('h3').children('.shares');
				var url = $storyOverlayInner.data('url');
				var storyId = $storyOverlayInner.data('id');
				// http://api-public.addthis.com/url/shares.json?url=
				$.ajax({
					url: "http://api-public.addthis.com/url/shares.json",
					type: 'GET',
					dataType: 'jsonp',
					data: {
						url: url
					},
					success: function (res) {
						var shares = res.shares;
						sharesNumberSpan.text(shares + "");
					}
				});

			},
			render: function() {
				var templ = _.template(this.template);
				this.$el.html(templ(this.model.toJSON()));
				this.renderClasses();

				return this;
			},
			renderClasses: function() {
				var $thisInner = this.$el.find('.inner'),
					$size = $thisInner.data('size'),
					$state = $thisInner.data('state'),
					$topic = $thisInner.data('topic'),
					$newest = $thisInner.data('newest');
					$language = $thisInner.data('language');
					$video = $thisInner.data('video');
					$specialVideo = $thisInner.data('specialVideo'),
					$zipcode = $thisInner.data('zipcode');

				this.$el.addClass($size + " " + $state + " " + $topic + " " + $language + " " + $zipcode);

				if ( $newest != '' ) {
					this.$el.addClass($newest);
				}
				if ( $video != '' ) {
					this.$el.addClass('video');
				}
				if ( $specialVideo == 'video' ) {
					this.$el.addClass('video');
				}
			}
		});



		var storyCollectionView = Backbone.View.extend({
	  		el: $('.story-wall'),
	  		limit : 50,
	  		events: {
	  			'click .share' : 'shareStory',
	  			'click .filters a' : 'filterClick',
	  			'click .sort' : 'openSort',
	  			'click .close' : 'closeOverlay',
	  			'click .load-more' : 'onClickMore',
	  			'click .share' : 'shareStory',
				'click .relate' : 'iCanRelate',
				'click .prev' : 'prevStory',
				'click .next' : 'nextStory',
				'click .radius-search' : 'toggleRadiusSearch',
				'submit #story-wall-radius-search' : 'radiusSearch',
				'focus #story-wall-radius-search .search-for' : 'focusRadiusSearch'
	  		},


	  		initialize: function() {
	    		this.collection = storyCollection;
	    		this.$isotopeContainer = $('.story-wall-isotope');
	    		this.on('render', this.afterRender);
	    		this.render();
	    		$window.on('resize', this.isotopeResize);
	  		},
	  		render: function() {
	  			var $i = 0;

	  			function storyWallWidth() {
	  				return $window.width() >= 768 ? 195 : 150;
	  			}

	  			var	opt = {
					layoutMode: 'perfectMasonry',
					perfectMasonry: {
						layout: 'vertical',
						liquid: false,
						columnWidth: storyWallWidth(),
						rowHeight: 165,
						minCols: 3
					},
					getSortData: {
						relates: function ($elem) {
							var elRelates = $($elem).children('.inner').attr('data-relates') || 0;
							return elRelates;
						},
						origSort: function ($elem) {
							var order = $($elem).children('.inner').attr('data-orig-sort') || 0;
							return order;
						}
					}
				};

				var items = this.collection.first(this.limit);

	    		_.each(items, function(item) {
	    			this.renderItem(item);
	    			smallBigShareBoxes($i);
	    			$i++;
	    		}, this);
	    		this.count = items.length;

	    		// for when a page is loaded with an id in the url that isn't in the first batch
	    		var segment_str = window.location.href;
				var segment_array = segment_str.split( '/' );
				var currentID = segment_array.pop();

	    		if ( isNaN(currentID) )  {
	    			return;
	    		}
	    		else {
	    			console.log('i have an id in my url');
	    			var $thisItem = $('.story-wall-isotope .item').find("[data-id='" + currentID + "']");
					if ( $thisItem.length <= 0 ) {
						console.log('i am not in the first load');
		    			this.collection.each(function(item) {
						    if ( item.attributes['ID'] == currentID ) {
						    	var storyView = new StoryView({ model: item });
								$('.story-wall-isotope').append(storyView.render().el);
			    			}
						});
					}
	    		}

		    	this.$el.removeClass('first');
		    	this.$isotopeContainer.isotope(opt);

				this.trigger('render');

			},
			afterRender: function () {
				$.each($('.copy h2.topic span'), function () {
					var $this = $(this),
					$topic = $this.closest('.inner').data('topic');
					$this.html( hopeIdeaFear($topic) );
				});

				$.each($('.copy .links .share'), function () {
					// http://api-public.addthis.com/url/shares.json?url=
					var url = $(this).data('url');
					var storyId = $(this).data('id');
					var that = $(this);
					$.ajax({
						url: "http://api-public.addthis.com/url/shares.json",
						type: 'GET',
						dataType: 'jsonp',
						data: {
							url: url
						},
						success: function (res) {
							var shares = res.shares;
							var sharesSpan = that.parent('.links').siblings('h3').children('#story' + storyId + 'shares');
							sharesSpan.text(shares + "");
						}
					})
				});
			},
			renderItem: function(item) {
				var storyView = new StoryView({ model: item });
				this.$el.find('.story-wall-isotope').append(storyView.render().el);

			},
			iCanRelate: function (e) {
				e.preventDefault();

				var clickedStoryId = $(e.target).data('id');
				var clickedStoryTopic = $(e.target).data('topic');

				$.ajax({
					url: '/wp-admin/admin-ajax.php',
					type: 'POST',
					data: 'action=user_can_relate&ip_address=' + currentIpAddress + '&post_id=' + clickedStoryId + '&story_type=' + clickedStoryTopic,
					success: function (data) {
						if (JSON.parse(data).success) {
							var currentRelates = parseInt($('#story' + clickedStoryId + 'relates').text());
							$('#story' + clickedStoryId + 'relates').text(currentRelates + 1);
							var peoplePerson = $('#story' + clickedStoryId + 'relates').siblings('span.people-person');
							if (currentRelates + 1 != 1 && peoplePerson != 'people') {
								peoplePerson.text('people');
							} else {
								peoplePerson.text('person');
							}
						}
					}
				});
			},
			shareStory : function(e) {
				e.preventDefault();
				$('.share-overlay').fadeIn(500);
				$body.addClass('overflow-hidden');

				$targetEl = $(e.target);

				// $('meta[property=\'og:title\']').attr('content', $targetEl.data('title'));
				// $('meta[property=\'og:url\']').attr('content', $targetEl.data('url'));
				// $('meta[property=\'og:image\']').attr('content', $targetEl.data('image'));
				// $('meta[property=\'og:video\']').attr('content', $targetEl.data('video'));
				// // $('meta[property=og:description]').attr('content', $targetEl.data('text'));

				$toolbox = $('.share-overlay .addthis_sharing_toolbox');
				$toolbox.attr('data-url', $targetEl.data('url'));
				$toolbox.attr('data-title', $targetEl.data('title'));
				config = $.extend(true, {}, window.addthis_config);
				// share = $.extend(true, {}, window.addthis_share);
				// share.url = $targetEl.data('url');
				// share.title = $targetEl.data('title');

				addthis.update('share', 'url', $targetEl.data('url'));
				addthis.update('share', 'title', $targetEl.data('title'));

				// addthis.toolbox('.share-overlay .addthis_sharing_toolbox', config, share);
				// addthis.layers.refresh();
			},
			filterClick : function(e) {
				e.preventDefault();
				// if it's a child that's clicked on
				if ($(e.target).closest('li').hasClass('sub')) {
					$(e.target).addClass('active').closest('li').siblings().find('a').removeClass('active');
					$(e.target).closest('ul').parent().addClass('active').siblings().removeClass('active').find('.sub a').removeClass('active');
				}
				else {
					// if it's a blank li it's clicked on
					if ( $(e.target).hasClass('blank') ) {
						var $thisTarget = $(e.target);
						if ( $thisTarget.siblings().hasClass('open') ) {
							$thisTarget.siblings().removeClass('open').slideUp(500);
						}
						else {
							$thisTarget.siblings().addClass('open').slideDown(500);

						}
					}
					$(e.target).closest('li').addClass('active').siblings().removeClass('active').find('.sub a').removeClass('active');
				}

				if ($(e.target).data('filter')) {
					var filterValue = $(e.target).data('filter');
					if (filterValue == '*') {
						this.$isotopeContainer.isotope({ filter: '*', sortBy: 'origSort' });
						mobileFilterSlideUp();
					}
					else {
						this.$isotopeContainer.isotope({ filter: '.' + filterValue });
						mobileFilterSlideUp();
					}
				} else if ($(e.target).data('sort-by')) {
					this.$isotopeContainer.isotope({
						sortBy: $(e.target).data('sort-by'),
						sortAscending: false
					});
					mobileFilterSlideUp();
				}

				function mobileFilterSlideUp() {
					if ( $window.width() < 1024 ) {
						$('.filters ul').slideUp(200).removeClass('open');
					}
				}




			},
			openSort : function(e) {
				var $thisTarget = $(e.target);
				if ( $thisTarget.siblings().hasClass('open') ) {
					$thisTarget.siblings().removeClass('open').slideUp(500);
				}
				else {
					$thisTarget.siblings().addClass('open').slideDown(500);

				}
			},
			/****** Zip code filter *****/
			removeSearchResults : function() {
/*			var searchResults = this.storyView.where({search_result : true});
			if (searchResults.length > 0) {
				_.each(searchResults, function(result, index) {
					result.set('search_result', false);
				});
			}*/
		// var searchResults = this.storyView.where({search_result : true});
			// if (searchResults.length > 0) {
			// 	_.each(searchResults, function(result, index) {
			// 		result.set('search_result', false);
			// 	});
			// }
			},

			toggleRadiusSearch : function(event) {
				this.$el.find('#story-wall-radius-search .search-for').focus();
				event.stopPropagation();
			},
			submitSearch : function(event) {
				$(event.target).closest('form').submit();
			},

			radiusSearch : function(event) {

				event.preventDefault();

				var view = this,
				value = $('.search-for').val();

				$.ajax({
					type : 'post',
					url : '/wp-admin/admin-ajax.php',
					data : {action : 'kb_search_zip_radius', zipcode : value},
					dataType : 'json',

					success: function(response) {

						return;

						var zipiArray =json_encode($jsonBlockResults);
						console.dir(zipiArray);
						console.log(zipiArray);
						if (response.error == true) {
						// NO RESULTS
						return;
					}
					// view.removeSearchResults();
					// var searchResults = new app.story_wall_block_collection(response);

					// searchResults.each(function(result, index) {
					// 	var blockToAdd;
					// 	var found = view.storyView.where({post_id : parseInt(result.get('post_id'))});
					// 	if (found.length == 0) {
					// 		found = view.collection.where({post_id : parseInt(result.get('post_id'))});
					// 		if (found.length == 0) {
					// 			// block not found in either storyView and not yet displayed collection
					// 			blockToAdd = result;
					// 		} else {
					// 			// block found in the collection of blocks that are not yet displayed.  remove it before we add to the storyView
					// 			blockToAdd = view.collection.remove(found[0]);
					// 		}
					// 		// block was not found in displayed blocks, add it.
					// 		view.storyView.add(blockToAdd);
					// 	} else {
					// 		// block found already displayed in displayedCollection
					// 		blockToAdd = found[0];
					// 	}
					// 	blockToAdd.set('search_result', true);
					// });
					// view.filterIsotope('.search-result');
				},

				error : function(text, error) {
				}

			});

			},


			/****** ./zip code filter *****/
			prevStory : function(e) {
	  			e.preventDefault();

	  			var $storyWallItem = $('.story-wall-isotope .item'),
	  				$storyWallItemOpen = $('.story-wall-isotope .item.open');

	  			function prevAction($prev) {
	  				if ($prev.length) {
					    $prev.addClass('open');
					}
					else {
						$storyWallItem.last().addClass('open').siblings().removeClass('open');
					}
	  			}

	  			// if prev is share link, skip
	  			if ( $storyWallItemOpen.prev().find('a').hasClass('share-your-own') ) {
	  				var $prev = $storyWallItemOpen.removeClass('open').prev().prev();
	  				prevAction($prev);
	  			}
	  			else {
	  				var $prev = $storyWallItemOpen.removeClass('open').prev();
	  				prevAction($prev);
	  			}

	  			$('.story-overlay .story-inner').fadeOut(500, function() {
	  				$(this).remove();
	  				$('.story-wall-isotope .item.open').find('.copy h2 a').trigger('click');
	  			});

	  		},
	  		nextStory : function(e) {
	  			e.preventDefault();

	  			var $storyWallItem = $('.story-wall-isotope .item'),
	  				$storyWallItemOpen = $('.story-wall-isotope .item.open');

	  			function nextAction($next) {
	  				if ($next.length) {
					    $next.addClass('open');
					}
					else {
						$storyWallItem.eq(0).addClass('open').siblings().removeClass('open');
					}
	  			}

	  			// if next is share link, skip
	  			if ( $storyWallItemOpen.next().find('a').hasClass('share-your-own') ) {
	  				var $next = $storyWallItemOpen.removeClass('open').next().next();
	  				nextAction($next);
	  			}
	  			else {
	  				var $next = $storyWallItemOpen.removeClass('open').next();
	  				nextAction($next);
	  			}

	  			$('.story-overlay .story-inner').fadeOut(500, function() {
	  				$(this).remove();
	  				$('.story-wall-isotope .item.open').find('.copy h2 a').trigger('click');
	  			});

	  		},
			closeOverlay : function(e) {
				e.preventDefault();
				$('.story-overlay').fadeOut(500).empty();
				$body.removeClass('overflow-hidden position-fixed');
				$('.story-wall-isotope .item.open').removeClass('open');
			},
			onClickMore: function(e) {
				var $i = 0,
	  				items = this.collection.models.slice(this.count, this.count + this.limit);

			    if (items.length) {
			        _.each(items, function(item) {
		    			this.renderItem(item);
		      			smallBigShareBoxes($i);
		      			$i++;
	    			}, this);
			        this.count += items.length;

			        this.$isotopeContainer.isotope('reloadItems').isotope();

			        this.afterRender();
			    }
			    else {
			        $('#more-button .load-more').attr('disabled', 'disabled');
				}
			},
			isotopeResize: function() {
				var	opt = {
					layoutMode: 'perfectMasonry',
					perfectMasonry: {
						layout: 'vertical',
						liquid: false,
						columnWidth: $window.width() >= 768 ? 195 : 150,
						rowHeight: 165,
						minCols: 3
			}
				};
				$('.story-wall-isotope').isotope(opt);
			}
		});

		var story = new storyCollectionView();

		var StoryRouter = Backbone.Router.extend({
	        routes: {
	        	'hope/:id': 'getStoryWallPopupHope',
	        	'idea/:id': 'getStoryWallPopupIdea',
	        	'fear/:id': 'getStoryWallPopupFear'
	        },
	    	getStoryWallPopupHope: function(id) {
	    		var $item = $('.story-wall-isotope .item').find("[data-id='" + id + "'][data-topic='hope']");
				// if ( $item.length > 0 ) {
					$item.find('.copy h2 a').trigger('click');
				// }
			},
			getStoryWallPopupIdea: function(id) {
				var $item = $('.story-wall-isotope .item').find("[data-id='" + id + "'][data-topic='idea']");
				// if ( $item.length > 0 ) {
					$item.find('.copy h2 a').trigger('click');
				// }
			},
			getStoryWallPopupFear: function(id) {
	    		$('.story-wall-isotope .item').find("[data-id='" + id + "'][data-topic='fear']").find('.copy h2 a').trigger('click');
			}
	    });

		var storyRouter = new StoryRouter;
    	Backbone.history.start();
    	storyRouter.navigate(location.hash, true);

	}

	// add desktop class to filters
	var $filtersFirst = $('.filters > ul'),
		$filters = $('.filters ul');
	$window.resize(function() {
		if ( $window.width() >= 1024  ) {
			if ( !$filtersFirst.hasClass('desktop') ) {
				$filtersFirst.addClass('desktop');
				$filters.slideDown(200);
			}
		}
		else {
			if ( $filtersFirst.hasClass('desktop') ) {
				$filtersFirst.removeClass('desktop');
				$filters.slideUp(200);
			}
		}
	});


	function smallBigShareBoxes($i) {
		//every 5 items add a 'share' block
		if ($i % 25 == 0) {
			// even is big, odd is small
			if ( $i % 2 == 0 ) {
				$('.story-wall-isotope').append('<div class="item large"><a href="" class="inner share-your-own open-form-overlay"><div class="image"></div><span>Share Your Voice</span></a></div>');
			}
			else {
				$('.story-wall-isotope').append('<div class="item small"><a href="" class="inner share-your-own small open-form-overlay"><div class="image"></div><span>Share Your Voice</span></a></div>');
			}
		}
	}

	function hopeIdeaFear(topic) {
		title = '';
		switch (topic) {
			case 'hope':
				title = 'I Hope';
			break;
			case 'idea':
				title = 'My Idea';
			break;
			default:
			case 'fear':
				title = 'I&#039;m Afraid';
			break;
		}

		return title;
	}


	/****************************************
		Video Embed Code Resizing
	****************************************/

	var $content = $('.iframe-wrap'),
		$content_iframes = $content.find('iframe');

	$content_iframes.each(function() {
		var $iframe = $(this),
			iframe_width = $iframe.attr('width') && $iframe.attr('width').match(/^[0-9]+(px)?$/) ? parseInt($iframe.attr('width'), 10) : $iframe.width(),
			iframe_height = $iframe.attr('height') && $iframe.attr('height').match(/^[0-9]+(px)?$/) ? parseInt($iframe.attr('height'), 10) : $iframe.height();
		$iframe.data('original-width', iframe_width);
		$iframe.data('original-height', iframe_height);
		$iframe.width(iframe_width).height(iframe_height);
	});

	$window.resize(function() {
		var content_width = $content.width();
		$content_iframes.each(function() {
			var $iframe = $(this),
				iframe_width = Math.min($iframe.data('original-width'), content_width),
				iframe_height = iframe_width * $iframe.data('original-height') / $iframe.data('original-width');
			$iframe.width(iframe_width).height(iframe_height);
		});
	});


	/****************************************
		Front Page
	****************************************/
	var $frontPage = $('#homepage');
	if ($frontPage) {
		$('.relate').click(function (e) {
			e.preventDefault();

			var clickedStoryId = $(this).data('id');
			var clickedStoryTopic = $(this).data('topic');

			$.ajax({
				url: '/wp-admin/admin-ajax.php',
				type: 'POST',
				data: 'action=user_can_relate&ip_address=' + currentIpAddress + '&post_id=' + clickedStoryId + '&story_type=' + clickedStoryTopic,
				success: function (data) {
					if (JSON.parse(data).success) {
						var currentRelates = parseInt($('#story' + clickedStoryId + 'relates').text());
						$('#story' + clickedStoryId + 'relates').text(currentRelates + 1);
						var peoplePerson = $('#story' + clickedStoryId + 'relates').siblings('span.people-person');
						if (currentRelates + 1 != 1 && peoplePerson != 'people') {
							peoplePerson.text('people');
						} else {
							peoplePerson.text('person');
						}
					}
				}
			});
		});

		$('.share').click(function (e) {
			e.preventDefault();
			$('.share-overlay').fadeIn(500);
			$body.addClass('overflow-hidden');

			$targetEl = $(this);
			$toolbox = $('.share-overlay .addthis_sharing_toolbox');
			$toolbox.attr('data-url', $targetEl.data('url'));
			$toolbox.attr('data-title', $targetEl.data('title'));
			config = $.extend(true, {}, window.addthis_config);

			addthis.update('share', 'url', $targetEl.data('url'));
			addthis.update('share', 'title', $targetEl.data('title'));
		});

		$.each($('.links .share'), function () {
			// http://api-public.addthis.com/url/shares.json?url=
			var url = $(this).data('url');
			var storyId = $(this).data('id');
			var that = $(this);
			$.ajax({
				url: "http://api-public.addthis.com/url/shares.json",
				type: 'GET',
				dataType: 'jsonp',
				data: {
					url: url
				},
				success: function (res) {
					var shares = res.shares;
					var sharesSpan = that.parent('.links').siblings('.copy').children('h3').children('span#story' + storyId + 'shares');
					sharesSpan.text(shares + "");
				}
			})
		});
	}


	$('.share-after-submit').click(function (e) {
		e.preventDefault();
		$('.share-overlay').fadeIn(500);
		$body.addClass('overflow-hidden');

		$targetEl = $(this);
		$toolbox = $('.share-overlay .addthis_sharing_toolbox');
		$toolbox.attr('data-url', $targetEl.data('url'));
		$toolbox.attr('data-title', $targetEl.data('title'));
		config = $.extend(true, {}, window.addthis_config);

		addthis.update('share', 'url', $targetEl.data('url'));
		addthis.update('share', 'title', $targetEl.data('title'));
	});


	/****************************************
		Timeline Page
	****************************************/
	var $timeline = $('#page-timeline');
	if ($timeline) {
		$( function() {
			$('.timeline-blocks').isotope({
				itemSelector: '.timeline-block',
				masonry: {
					columnWidth: 100,
					gutter: 10
				}
			});
		});

		// timelineWaypoints is defined in a PHP loop in template-timeline.php
		// below ul#timeline-nav-list
		if ('undefined' !== typeof timelineWaypoints) {
			for (var timelineWaypoint in timelineWaypoints) {
				var theWaypoint = $(timelineWaypoints[timelineWaypoint]).waypoint(function (direction) {
					var currentActive = $('#timeline-nav ul li a span.active');
					currentActive.removeClass('active');
					var currentActiveIcon = currentActive.children('img');
					currentActiveIcon.attr('src', currentActiveIcon.data('icon-small')).css('margin-top', '0px');

					var navIcon = $('#timeline-nav ul li a[href=#' + this.element.id + '] span');
					navIcon.addClass('active');
					var navIconImg = navIcon.children('img');
					navIconImg.attr('src', navIconImg.data('icon-large')).css('margin-top', '-15px');
				}, { offset: '300px;' });
			}
		}

		/* timeline navigation that scrolls with the page
			once you get down to it */
		$(document).ready(function () {
			// check if it's there to remove js error on all pages
			if ( $('#timeline-nav').length ) {
				$('#timeline-nav').scrollFix({
					fixTop: 200,
					fixOffset: -200,
					unfixOffset: -200
				});
			}

			$('div#timeline-nav li a').hover(
				function () {
					$(this).children('span.icon').each(function() {
						if (!$(this).hasClass('active')) {
							var thisImg = $(this).children('img');
							thisImg.attr('src', thisImg.data('icon-large')).css('margin-top', '-15px');
						}
					});
				},
				function () {
					$(this).children('span.icon').each(function() {
						if (!$(this).hasClass('active')) {
							var thisImg = $(this).children('img');
							thisImg.attr('src', thisImg.data('icon-small')).css('margin-top', '0px');
						}
					});
				}
			);

			$('div#timeline-nav li a').click(function () {
				// For the current (previous) nav item
				// remove the active class and change the image to the small icon
				var prevActive = $('div#timeline-nav li .active');
				var prevImg = prevActive.children('img');
				prevActive.removeClass('active');
				prevImg.attr('src', prevImg.data('icon-small'));

				// Now let's do the opposite for the current one. YAY!
				var newActive = $(this).children('span.icon');
				var currentImg = newActive.children('img');
				newActive.addClass('active');
				currentImg.attr('src', currentImg.data('icon-large'));
			});
		});
	}

	/* snap effect - buggy, or else i don't know what the hell i'm doing. probably the latter.

	most likely the latter.

	definitely the latter. */

	// if ( $timeline.length > 0 ) {
	// 	$body.panelSnap({
	// 		panelSelector: '.snap',
	// 		slideSpeed: 500,
	// 		directionThreshold: 100,
	// 	});
	// }





	/****************************************
		Form Overlay
	****************************************/

	var	$formOverlay = $('.form-overlay'),
		$formWrap = $formOverlay.find('.wrap'),
		$formWrapInner = $formWrap.find('.inner-wrap'),
		$formTitle = $formOverlay.find('.title-wrap'),
		$formButtonA = $formOverlay.find('button.regular'),
		$formButtonASubmit = $formOverlay.find('button[type="submit"]'),
		$formCount = $formOverlay.find('.close-count .count');

	$window.resize(function() {
		$formWrap.add($formWrapInner).height( $wHeight );
	}).trigger('resize');

	/* fake file upload click */

	$('.fake-photo-upload').click(function() {
		$('.photo-upload').trigger('click');
	});

	$('input[type=file]').change(function(e) {
		// $oldVal = $(this).val();
		var path = $(this).val();
 		var filename = path.replace(/^.*\\/, "");
		$('.fake-photo-upload-name p').html( filename );
	});

	$('.fake-video-upload').click(function() {
		$(this).fadeOut(200, function() {
			$(this).closest('.inner-wrap').find('.video-upload').fadeIn(300);
		});
	});

	/* select styling plugin */

	$('select').selectBox({
		mobile: true,
		menuTransition: 'fade'
	});

	/* open overlay */

	$('.open-form-overlay').click(function(e) {
		e.preventDefault();
		$formOverlay.fadeIn(500);
		$body.addClass('overflow-hidden');
		if ( $formOverlay.hasClass('first') ) {
			$formWrapInner.eq(0).addClass('active').fadeIn(500);
			$formOverlay.removeClass('first');
		}
	});

	/* close overlay */

	$('.close-form').click(function(e) {
		e.preventDefault();
		$body.removeClass('overflow-hidden');
		$formOverlay.fadeOut(200);
		// reset form
		$('#form-voice')[0].reset();
		// reset disabled inputs
		$formOverlay.find('.inner-wrap.name-location input[type="text"]').prop('disabled', false);
		$('.form-overlay .wrap .inner-wrap.name-location select').selectBox('enable');
		// make first section active
		$('.form-overlay .wrap .inner-wrap.intro').addClass('active').siblings().removeClass('active');
		$formWrap.animate({scrollTop : 0}, 200);
		// set things back to normal
		formTitleFadeOut();
		$formButtonA.find('.text').html('Next').fadeIn(200).siblings().fadeOut(200).closest('.button').removeClass('submit');
		$formButtonASubmit.fadeOut(200);
		$formOverlay.find('.back').fadeOut(200);
		// select box stuff
		$('select').selectBox('refresh');
		// reset number counter
		$formCount.removeClass('show').find('span').html('0');
	});

	$formButtonA.click(function(e) {
		e.preventDefault();

		if ( $formOverlay.find('.inner-wrap.active').hasClass('intro') ) {
			$formOverlay.find('.back').fadeIn(200);
			$(this).find('.text').fadeOut(200).siblings().fadeIn(200);
		}
		else if ( $formOverlay.find('.inner-wrap.active').hasClass('name-location') ) {
			// if nothing is filled in don't progress
			var $count = 0,
				$nameLocationInput = $formWrap.find('.name-location input[type="text"]'),
				$anonymousCheckbox = $formWrap.find('.anonymous input[type="checkbox"]');
			$nameLocationInput.each(function() {
				if ( !$(this).val() ) {
					$count++;
				}
			});
			if( ( $count > 0 ) && ( !$anonymousCheckbox.is(':checked') ) ) {
				$('.error').html('Please fill in the above information, or click on the checkbox to remain anonymous.').fadeIn(200);
				setTimeout(function() {
				     $('.error').fadeOut(200);
				}, 2000);
				return;
			}
			else {
				formTitleFadeIn();
			}

		}
		else if ( $formOverlay.find('.inner-wrap.active').hasClass('topic') ) {
			slurValidating();
			reviewFormInfoPresent();
			if ( slurValidating() == true ) {
				return;
			}
			else if ( ($formOverlay.find('.inner-wrap.active.topic textarea').val() != '') && ($formOverlay.find('.inner-wrap.active.topic .topic-first input[type="text"]').val() == '') ) {
				$('.error').html('Please fill in the title field.').fadeIn(200);
				setTimeout(function() {
				     $('.error').fadeOut(200);
				}, 2000);
				return;
			}
			else if ( $formOverlay.find('.inner-wrap.active.topic .video-upload').val() != '' ) {
				// validate video
				var vidVal = $formOverlay.find('.inner-wrap.active.topic .video-upload').val(),
					re = new RegExp('(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})');
				if ( !re.test(vidVal) ) {
					$('.error').html('Please enter a correct YouTube url.').fadeIn(200);
					setTimeout(function() {
					     $('.error').fadeOut(200);
					}, 2000);
				    return
				}
			}
			else {
				if ( $formOverlay.find('.inner-wrap.active').hasClass('idea') ) {
					if ( reviewFormInfoPresent() == 3 ) {
						$('.error').html('Please fill out one topic to continue.').fadeIn(200);
						setTimeout(function() {
						     $('.error').fadeOut(200);
						}, 2000);
						return;
					}
					else {
						formTitleFadeOut();
						$formButtonA.find('.text').fadeOut(100).siblings().fadeIn(100);
					}
				}
			}
		}
		else if ( $formOverlay.find('.inner-wrap.active').hasClass('confidential-ques') ) {
			if ( demoSelectCheck() > 0 ) {
				demoError();
				return;
			}
			else if ( $formOverlay.find('.inner-wrap.active .zipcode-input').val() == '' ) {
				demoError();
				return;
			}
			else {
				var $checksArray = [];
				$('.inner-wrap.active .checks').each(function() {
					var $i = 0;
					$(this).find('input[type="checkbox"]').each(function() {
						if ( $(this).is(':checked') ) {
				        	$i++;
				        }
				    });
				    $checksArray.push($i);
				});
				if ( jQuery.inArray(0, $checksArray) != -1 ) {
					demoError();
					return;
				}
			}

			$formButtonA.fadeOut(200, function() {
				$formButtonASubmit.fadeIn(200).closest('.button').addClass('submit');
			});
			reviewForm();
			$('.form-img').cropbox({
		        width: 380,
		        height: 320
		    });
		}
		else if ( $formOverlay.find('.inner-wrap.active').hasClass('thank-you') ) {
			window.location = '/story-wall';
			return;
		}

		var $scrollAmount = ($formOverlay.find('.inner-wrap.active').index() + 1) *  $wHeight;
		// add active to next block
		$formOverlay.find('.inner-wrap.active').removeClass('active').next().addClass('active');
		// scroll to next block
		$formWrap.animate({scrollTop : $scrollAmount});

		// after new active class adjust things on topic sections
		newTopicStuff();

		// change pager #
		slideNumbers();
	});

	/* submit button in form */

	$formButtonASubmit.click(function() {

		// fade in topic share
		$formOverlay.find('.inner-wrap.review .column.active').each(function() {
			var $topic = $(this).data('type');
			$('.email-share').find('.' + $topic).addClass('active');
		});

		var $scrollAmount = ($formOverlay.find('.inner-wrap.active').index() + 1) *  $wHeight;
		// add active to next block
		$formOverlay.find('.inner-wrap.active').removeClass('active').next().addClass('active');
		// scroll to next block
		$formWrap.animate({scrollTop : $scrollAmount});

		// change pager #
		slideNumbers();

		$formButtonASubmit.fadeOut(200, function() {
			$formButtonA.fadeIn(200).find('.text').fadeIn(100).text('Follow the Conversation').siblings().fadeOut(100).closest('.button');
		});

	});

	$formOverlay.find('.back').click(function(e) {
		e.preventDefault();
		if ( $('.inner-wrap.active').hasClass('name-location') ) {
			// if nothing is filled in don't progress
			var $count = 0,
				$nameLocationInput = $formWrap.find('.name-location input[type="text"]'),
				$anonymousCheckbox = $formWrap.find('.anonymous input[type="checkbox"]');
			$nameLocationInput.each(function() {
				if ( !$(this).val() ) {
					$count++;
				}
			});
			if( ( $count > 0 ) && ( !$anonymousCheckbox.is(':checked') ) ) {
				$('.error').html('Please fill in the above information, or click on the checkbox to remain anonymous.').fadeIn(200);
				setTimeout(function() {
				     $('.error').fadeOut(200);
				}, 2000);
				return;
			}
			else {
				$(this).fadeOut(300);
			}
		}
		else if ( $formOverlay.find('.inner-wrap.active').hasClass('confidential-ques') ) {
			formTitleFadeIn();
		}
		else if ( $formOverlay.find('.inner-wrap.active').hasClass('topic') ) {
			slurValidating();
			if ( slurValidating() == true ) {
				return;
			}
			else {
				if ( $formOverlay.find('.inner-wrap.active').hasClass('hope') ) {
					formTitleFadeOut();
					$formButtonA.find('.text').fadeOut(200).siblings().fadeIn(200);
				}
			}
		}
		else if ( $formOverlay.find('.inner-wrap.active').hasClass('review') ) {
			$formButtonASubmit.fadeOut(100, function() {
				$formButtonA.fadeIn(100).find('.text').fadeOut(100).siblings().fadeIn(100).closest('.button').removeClass('submit');
			});
			$formWrap.find('.review .column').each(function() {
				$(this).removeClass('active').fadeOut(100);
			});
		}
		else if ( $formOverlay.find('.inner-wrap.active').hasClass('thank-you') ) {
			$formButtonA.fadeOut(100, function() {
				$formButtonASubmit.fadeIn(100);
			});

		}
		var $scrollAmount = ($formOverlay.find('.inner-wrap.active').index() - 1) *  $wHeight;
		// add active to next block
		$formOverlay.find('.inner-wrap.active').removeClass('active').prev().addClass('active');
		// scroll to next block
		$formWrap.animate({scrollTop : $scrollAmount});

		// after new active class adjust things on topic sections
		newTopicStuff();

		// change pager #
		slideNumbers();
	});

	$window.resize(function() {
		var $scrollAmount = ($formOverlay.find('.inner-wrap.active').index()) * $wHeight;
		$formWrap.animate({scrollTop : $scrollAmount}, 1);
	});

	/* diable inputs if anonymous */

	$formOverlay.find('input[type="checkbox"]').click(function() {
		var $anonymous = $(this).closest('.anonymous');
		if ( $anonymous.hasClass('active') ) {
			$anonymous.removeClass('active');
			$('.form-overlay .wrap .inner-wrap.name-location select').selectBox('enable');
		}
		else {
			$anonymous.addClass('active');
			$('.form-overlay .wrap .inner-wrap.name-location select').selectBox('disable');
		}
		$formOverlay.find('.inner-wrap.name-location input[type="text"]').prop("disabled", function(i, v) { return !v; });
	});

	/* when topic titles are entered adjust button text */

	$formOverlay.find('.inner-wrap.topic input[type="text"]').change(function() {
		$formButtonA.find('.text').fadeOut(200).siblings().fadeIn(200);
	});

	/* topic links click */

	$('.form-overlay .title-wrap a').click(function(e) {
		e.preventDefault();

		slurValidating();
		if ( slurValidating() != true ) {
			$(this).addClass('main').siblings().removeClass('main');

			var $type = $(this).data('type');
			// check for slurs

			// scroll to that section
			$formOverlay.find('.inner-wrap.active').removeClass('active');
			$formWrap.find('.inner-wrap.' + $type).addClass('active');

			var $scrollAmount = ($formOverlay.find('.inner-wrap.active').index()) *  $wHeight;
			$formWrap.animate({scrollTop : $scrollAmount});

			// change pager #
			slideNumbers();
		}
	});

	/* if other is clicked in demographic section show popup */

	// selects
	$('select').change(function() {
		var $this = $(this);
		if ( ($this.hasClass('has-other')) && ($this.find('option:selected').text() == 'Other') ) {
			$this.closest('.multiple').find('.other-hidden').fadeIn(300);
		}
	});

	// checkboxes
	$('input[type=checkbox]').change(function() {
		var $this = $(this);
		if ( $this.hasClass('has-other') ) {
			if ( $this.attr('checked') ) {
				$this.closest('.multiple').find('.other-hidden').fadeIn(300);
			}
		}
	});

	$('.other-submit').click(function() {
		var $this = $(this),
			$otherValue = $this.siblings().val();
		$this.closest('.other-hidden').fadeOut(300);
		$this.closest('.multiple').find('input.has-other').val($otherValue);
	});

	/* edit click in confirmation section */

	$('.edit').click(function(e) {
		e.preventDefault();
		var $type = $(this).closest('.column').data('type');

		$formOverlay.find('.inner-wrap.active').removeClass('active');
		$formWrap.find('.inner-wrap.' + $type).addClass('active');

		var $scrollAmount = ($formOverlay.find('.inner-wrap.active').index()) * $wHeight;
			$formWrap.animate({scrollTop : $scrollAmount});

		formTitleFadeIn();
		$formTitle.find("[data-type='" + $type + "']").addClass('main').siblings().removeClass('main');

		slideNumbers();

		$formButtonASubmit.fadeOut(100, function() {
			$formButtonA.fadeIn(100).find('.text').fadeOut(100).siblings().fadeIn(100).closest('.button').removeClass('submit');
		});


	});

	/* if applicable images added to confirmation boxes */

	if (window.File && window.FileReader && window.FileList && window.Blob) {

	  	// this function is called when the input loads an image
		function renderImage(file){
			var reader = new FileReader();
			reader.onload = function(event){
				the_url = event.target.result;
				$formWrap.find('.inner-wrap.review .column .inner .background-image').html('<img class="form-img" src="' + the_url + '">');
			}
			reader.readAsDataURL(file);
		}

	  	// watch for change on the file upload
		$('.photo-upload').change(function() {
			renderImage(this.files[0]);
		});

	}

	/* character count for titles in topic section */

	$('.form-overlay .wrap .inner-wrap.topic .topic-first input[type="text"]').keyup(function() {
	    var $this = $(this),
	    	left = 40 - $(this).val().length;
	    if (left < 0) {
	        left = 0;
	    }
	    $this.closest('.topic-first').find('.counter').text(left);
	});

	/* checking for slurs in input and text area */

	function slurValidating() {
		var slurs = new Array("faggot", "fag", "fuck", "fucker", "fucking", "fuk", "fock", "fok", "fairy", "cunt", "bitch", "tranny", "trannies", "dyke", "dyk", "dike", "dick", "douche", "douchebags", "shit", "shite", "shyt", "motherfucker", "fuckers", "whore", "whores", "manwhore", "bastard", "bastards", "slut", "nigger", "nigga", "coon", "ass", "anal", "sodomy", "sodomist", "sodomizer", "pedos", "pedophile", "blowjob", "spic", "spics", "spik", "boner", "paki", "ape", "beaner", "beaney", "brownie", "sand nigger", "burrhead", "ching chong", "chinaman", "chink", "cholo", "cracker", "flip", "gook", "gringo", "hillbilly", "ike", "injun", "jap", "jigaboo", "jiggabo", "jigarooni", "jigger", "kaffir", "kike", "kyke", "niglet", "nig", "niggar", "raghead", "towelhead", "towel head", "redneck", "spick spik", "tar baby", "uncle tom", "carpet muncher", "lesbo", "leso", "lezzie", "muff diver", "bugger", "flamer", "flaming", "cock knocker", "cocknocker", "cockknocker", "sissy", "nancy boy", "nancy", "nellie", "fruit", "twink", "fellatio", "wank", "cock", "buttplug", "tit", "tits", "turd", "terd", "twat", "cocksucker", "pussy", "homo", "fudgepacker", "fudge packer", "maricón", "puto", "puta", "mariposo", "mariposa", "puñal", "maricones", "putos", "putas", "mariposos", "mariposas", "puñales", "marica", "maricas", "chingar", "joder", "hijo de la chingada", "hijo de su chingada madre", "hijo de su rechingada madre", "hija de la chingada", "hija de su chingada madre", "hija de su rechingada madre", "hijos de la chingada", "hijos de su chingada madre", "hijos de su rechingada madre", "hijas de la chingada", "hijas de su chingada madre", "hijas de su rechingada madre", "chingado", "chingados", "chingada", "chingadas", "jodido", "jodidos", "jodida", "jodidas", "cagado", "cagados", "cagada", "cagadas", "pinche", "pinches", "maldito", "maldita", "reinona", "maricón", "hadita", "perra", "zorra", "concha", "mico", "coño", "panocha", "conejo", "bruja", "cabrona", "guarra", "arpía", "víbora", "conchuda", "vestida", "lesbiana", "tortillera", "torta", "polla", "verga", "pinga", "cretino", "ojete", "sorete", "forro", "pelotudo", "pelotuda", "gacho", "gacha", "guacho", "guacha", "gilipollas", "repulsivo", "repulsiva", "forros", "pelotudos", "pelotudas", "gachos", "gachas", "guachos", "guachas", "repulsivos", "repulsivas", "mierda", "hijoputa", "hijo de puta", "hija de puta", "chingada madre", "conchatumadre", "conchetumadre", "comevieja", "follamadres", "prosti", "jodida", "jodido", "maldita", "maldito", "ramera", "fulana", "fulano", "buscona", "buscón", "gamberra", "guarra", "huila", "piruja", "putas", "putos", "prostis", "rameras", "fulanas", "fulanos", "busconas", "buscones", "gamberras", "guarras", "huilas", "pirujas", "prostiputo", "perro", "prostiputos", "perros", "cabrón", "cabrona", "desgraciado", "desgraciada", "putada", "cabronada", "cabrones", "desgraciados", "cabronas", "desgraciadas", "furcia", "zorrón", "mujerzuela", "congo", "conga", "negrata", "negro", "negra", "cabecita negra", "cabeza negra", "cola", "culo", "trasero", "imbécil", "coñazo", "sodomía", "sodomita", "sodomizar", "pedófilo", "pedófila", "parada", "parado", "dura", "duro", "tenerla parada", "tenerlo parado", "chango", "changa", "mono", "chino", "china", "oriental", "amarillo", "amarilla", "muerto de hambre", "muerta de hambre", "muertos de hambre", "muertas de hambre", "japo", "ponja", "rata judía", "moro", "mora", "moromierda", "cerril", "zafio", "zafia", "pueblerino", "pueblerina", "paleto", "paleta", "provinciano", "provinciana", "machorra", "tortillera", "marimacha", "marimacho", "macha", "cachapera", "pata", "bucha", "livais", "libais", "bollera", "camionera", "camionero", "machungo", "machunga", "arepera", "machorrona", "trailera", "trailero", "lencha", "machina", "marimar", "manflora", "nona", "talla alfombras", "machorras", "tortilleras", "tortas", "marimachas", "marimachos", "machas", "cachaperas", "patas", "buchas", "bolleras", "camioneras", "camioneros", "machungos", "machungas", "areperas", "machorronas", "traileras", "traileros", "lenchas", "machinas", "manfloras", "nonas", "lesbi", "dar por el", "dar por detroit", "muerde almohadas", "carajo", "putada", "amanerado", "amanerada", "con pluma", "con mucha pluma", "loca", "reinona", "sarasa", "cobarde", "nenaza", "afeminado", "afeminada", "pájara", "titera", "blandengue", "noño", "blandito", "blandita", "debilucho", "debilucha", "alfeñique", "felación", "mamada", "chupada", "pete", "chivo", "limpieza de sable", "hacer una mamada", "guagüis", "sexo oral", "chivo", "mamut", "guapos", "mamey", "paja", "manuela", "pajearse", "hacerse una paja", "masturbarse", "masturbación", "verga", "pija", "pito", "rabo", "nabo", "tapón anal", "dilatador anal", "estimulador anal", "consolador anal", "teta", "chichi", "goma", "tetas", "chichis", "gomas", "lolas", "zurullo", "zurrullo", "mojón", "sorete", "mierda", "serote", "cerote", "bosta", "churro", "zurrullón", "zurada", "cacota", "truño", "chorizo", "mierda", "cajeta", "pendejo", "pendeja", "opa", "chupavergas", "Chupapenes", "chupar verga", "chúpame la verga", "chupapollas", "chupar polla", "chúpame la polla", "mamón", "chupapija", "mamaguevo", "mamahuevo", "petero", "petera", "soplapollas", "soplapingas", "tragasables", "chocha", "crica", "chocho", "acción", "acostón", "polvo", "miedoso", "sacatón", "cobarde", "mamacita", "papaya", "loca", "trolo", "joto", "mariquita", "mariposón", "muscoloca", "marylycra", "circuitera", "puñetas", "locas", "trolos", "jotos", "mariquitas", "mariposones", "soplanucas", "naco", "naca", "indio", "india", "wey", "weyes", "guey", "güey", "gueyes", "güeyes", "tomar por culo");

		var userTextInputWords = $formWrap.find('.inner-wrap.active .topic-first input[type="text"]').val().toLowerCase().split(/\W+/),
			userTextAreaWords = $formWrap.find('.inner-wrap.active textarea').val().toLowerCase().split(/\W+/);

		for( var i in slurs) {
			if( ( userTextAreaWords.indexOf( slurs[i] ) != -1 ) || ( userTextInputWords.indexOf( slurs[i] ) != -1 ) ) {
			    $('.error').html('Please remove the bad word(s).').fadeIn(200);
				setTimeout(function() {
				     $('.error').fadeOut(200);
				}, 2000);
				return true;
		  	}
		}
	}

	function formTitleFadeIn() {
		$formTitle.fadeIn(200);
	}

	function formTitleFadeOut() {
		$formTitle.fadeOut(200);
	}

	function newTopicStuff() {
		var $newActive = $formOverlay.find('.inner-wrap.active');
		if ( $newActive.hasClass('topic') ) {
			// new title
			var $type = $newActive.data('type');
			$formTitle.find("[data-type='" + $type + "']").addClass('main').siblings().removeClass('main');
			// new button text
			if ( !$newActive.find('input[type="text"]').val() ) {
				$formButtonA.find('.text').html('Skip').fadeIn(200).siblings().fadeOut(200);
			}
			else {
				$formButtonA.find('.text').fadeOut(200).siblings().fadeIn(200);
			}
		}
	}

	function slideNumbers() {
		// each slide is equal to its index
		var $number = $formWrap.find('.inner-wrap.active').index();
		if ( $number == 0 ) {
			$formCount.removeClass('show');
		}
		else {
			$formCount.addClass('show');
		}
		$formCount.find('span').html($number);
	}

	function reviewFormInfoPresent() {
		// check to see if any topic has a title
		var $i = 0;
		$formWrap.find('.inner-wrap.topic').each(function() {
			if ( $(this).find('.topic-first input[type="text"]').val() == '' ) {
				$i++;
			}
		});
		return $i;
	}

	function reviewForm() {
		// get generic data from form
		var $name = $("input[name='name']").val(),
			$city = $("input[name='city']").val(),
			$state = $('#states option:selected').val();

		//for each topic, if title value exists, get title information and fade in that block with title info
		var $topics = ['hope', 'fear', 'idea'];
		$('.form-overlay .wrap .inner-wrap.topic').each(function(i) {
			var $title = $(this).find('.topic-first input[type="text"]').val();
			if ( $title != '' ) {
				var $thisTopic = $('.inner-wrap.review .content-wrap').find("[data-type='" + $topics[i] + "']");
				$thisTopic.addClass('active').fadeIn(100).find('.item .copy h2 span').html($title);
				if ( $('.anonymous').hasClass('active') ) {
					$thisTopic.find('.item .copy p .name').html('Anonymous');
				}
				else {
					$thisTopic.find('.item .copy p .name').html($name + ', ');
					$thisTopic.find('.item .copy p .city').html($city + ', ');
					$thisTopic.find('.item .copy p .state').html($state);
				}
			}
		});
		$window.trigger('resize');
	}

	function demoError() {
		$('.error').html('Please fill out all of the demographics.').fadeIn(200);
		setTimeout(function() {
		     $('.error').fadeOut(200);
		}, 2000);
	}

	function demoSelectCheck() {
		var $i = 0;
		$('.inner-wrap.active .selectBox').each(function() {
			if ( $(this).find('option:first').attr('selected') ) {
	            $i++;
	        }
		});
		return $i;
	}

	$window.resize(function() {
		var $thisWrapper = $('.inner-wrap.review .content-wrap .wrapper'),
			$length = $('.inner-wrap.review .content-wrap .column.active').length;
		if ( $window.width() > 1210 ) {
			$thisWrapper.width( $('.inner-wrap.review .content-wrap .column.active').width() * $length );
		}
		else if ( ($window.width() <= 1210) && ($window.width() > 820) ) {
			if ( $length == 1 ) {
				$thisWrapper.width(400);
			}
			else {
				$thisWrapper.width(800);
			}
			$('.inner-wrap.review .content-wrap').height( $window.height() - 100 );
		}
		else if ( ($window.width() <= 820) && ($window.width() >= 768) ) {
			$thisWrapper.width(400);
		}
		else {
			$thisWrapper.width(290);
		}
	}).trigger('resize');

});



/* animate logo on homepage after page load */

$(window).load(function() {
 	if ( $('#homepage').length ) {
 		$('.logo-homepage img').addClass('zoomIn animated').removeClass('opacity');
 	}
});






/* youtube videos */

var player;
function onYouTubePlayerAPIReady() {
	player = new YT.Player('player');
}








