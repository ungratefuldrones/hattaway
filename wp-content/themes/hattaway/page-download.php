<?php
	
	// =========================
	//	This presumes a page with name "Download"
	// ============================

	function array_flat($array, $prefix = '')
	{
	    $result = array();

	    foreach ($array as $key => $value)
	    {
	        $new_key = $prefix . (empty($prefix) ? '' : '.') . $key;

	        if (is_array($value))
	        {
	            $result = array_merge($result, array_flat($value, $new_key));
	        }
	        else
	        {
	            $result[$new_key] = $value;
	        }
	    }

	    return $result;
	}

	if ( is_user_logged_in() ) {
		
		global $siteurl;
		global $environment;

		$username = 'hfcdev';
		$password = 'st@tl3r';

		$url = trailingslashit($siteurl) . '/api/get_posts/?posts_per_page=-1';
		
		$context = null;
		
		if ($environment !== 'production') {  
			$context = stream_context_create(array(
			    'http' => array(
			        'header'  => "Authorization: Basic " . base64_encode("$username:$password")
			    )
			));
		}

		$data = file_get_contents($url, false, $context);
		
		if ( isset($_GET['type']) &&  $_GET['type'] == 'csv' ) {

			//wp_die('Not implemented.');

			/* set our content type to match the file we are downloading */
			header("Content-Type: text/html; charset=utf-8");
			/* Tell the browser how big the file is going to be */
			//header("Content-Length: ".filesize($filename)."\n\n");
			/* force the file to be downloaded */
			header("Content-Disposition: attachment; filename=posts.csv");
			/*
			$data = str_replace(
			 array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6"),
			 array("'", "'", '"', '"', '-', '--', '...'),
			 $data);
			
			// Next, replace their Windows-1252 equivalents.
			 $data = str_replace(
			 array(chr(145), chr(146), chr(147), chr(148), chr(150), chr(151), chr(133)),
			 array("'", "'", '"', '"', '-', '--', '...'),
			 $text);
			*/
	

			/* echo out the contents of the file */
			$array = json_decode($data, true);
			$f = fopen('php://output', 'w');
			
			ob_start();
			
			$firstLineKeys = false;
			
			foreach ($array['posts'] as $line)
			{
			    if (empty($firstLineKeys))
			    {
			        $firstLineKeys = array_keys(array_flat($line));
			        fputcsv($f, $firstLineKeys);
			        $firstLineKeys = array_flip($firstLineKeys);
			    }
			   // if (is_array($line)){
			    //	$line = implode(' / ', $line );
			   // }
			    // Using array_merge is important to maintain the order of keys acording to the first element
			    @fputcsv($f, @array_merge($firstLineKeys, array_flat($line)));
			}
			$string = ob_get_clean();

			echo $string;
			
			//var_dump(array_flat($array));
		
		} else {
			/* set our content type to match the file we are downloading */
			header("Content-Type: text/html; charset=utf-8");
			/* Tell the browser how big the file is going to be */
			//header("Content-Length: ".filesize($filename)."\n\n");
			/* force the file to be downloaded */
			header("Content-Disposition: attachment; filename=posts.json");

			/* echo out the contents of the file */
			echo $data;
		}

	} else {
		wp_die('Unauthorized.');
	}