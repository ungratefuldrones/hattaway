<?php get_header(); ?>

<?php if ( have_posts() ) :?>	
	<?php while ( have_posts() ) : the_post(); ?>

	<a href="/" class="top-title mobile-show"><?php _e('Our Tomorrow', 'hattaway'); ?></a>
	<div class="top-gradient"></div>
	
	<div class="page">
		<div class="container-alt">
			<div class="page-title">
				<h1><?php echo get_the_title(); ?> </h1>
			</div>
			<div class="page-content">
				<?php the_content(); ?>
			</div>
		</div>
	</div>

	<div class="bottom-sticky">
		<a href="" class="fl-left watch video home-video-click"><?php _e('Watch the Video', 'hattaway'); ?></a>
		<a href="" class="fl-right plus open-form-overlay"><span><?php _e('Share Your Voice', 'hattaway'); ?></span><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/icon-OrangePlusIcon_Small.png"></a>
	</div>

	<?php endwhile; ?>
<?php else : ?>	
<?php endif; ?>

<?php get_footer(); ?>
