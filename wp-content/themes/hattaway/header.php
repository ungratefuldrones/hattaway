<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>

	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="<?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?>" data-orig-content="<?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?>" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="<?php echo get_bloginfo('template_url'); ?>/assets/images/Logo.png" data-orig-content="<?php echo get_bloginfo('template_url'); ?>/assets/images/Logo.png" />
    <meta property="og:url" content="<?php echo get_the_permalink(); ?>" data-orig-content="<?php echo get_the_permalink(); ?>" />
    <meta property="og:video" content="" />
    <meta property="og:video:width" content="560" />
    <meta property="og:video:height" content="340" />
    <meta property="og:video:type" content="application/x-shockwave-flash" />
	<title><?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />

	<!-- Vinyl Font from Typekit -->
    <script src="//use.typekit.net/gpu0epw.js"></script>
    <script>try{Typekit.load();}catch(e){}</script>

    <!-- Gotham Font from Typography -->
    <link rel="stylesheet" type="text/css" href="//cloud.typography.com/7324852/723486/css/fonts.css" />

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/assets/dist/hattaway.all.css">

    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel="shortcut icon" href="<?php echo get_bloginfo('template_url'); ?>/assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo get_bloginfo('template_url'); ?>/assets/images/favicon.ico" type="image/x-icon">


    <?php wp_head(); ?>

</head>

<body <?php if (is_page('story-wall')) {?>class="story-wall-page"<?php } ?>>

<div id="wrapper" style="background-image:url(<?php echo get_bloginfo('template_url'); ?>/assets/images/OT-loader-sm.gif);"></div>
    <header class="main-header mobile-hide">
        <section class="left">
            <?php if ( is_front_page() ) { ?>
                <a href="/" class="logo-homepage"><img class="opacity hide" src="<?php echo get_bloginfo('template_url'); ?>/assets/images/Logo-homepage.png"></a>
            <?php } else  { ?>
                <a href="/"><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/Logo.png"></a>
            <?php } ?>
        </section>
        <section class="right">
            <div>
                <section class="social ">
                    <a target="_blank" href="<?php echo get_field('facebook', 'option');?>"><img class="facebook" src="<?php echo get_bloginfo('template_url'); ?>/assets/images/header-facebook.png"></a>
                    <a target="_blank" href="<?php echo get_field('twitter', 'option');?>"><img class="twitter" src="<?php echo get_bloginfo('template_url'); ?>/assets/images/header-twitter.png"></a>
                    <a target="_blank" href="<?php echo get_field('youtube', 'option');?>"><img class="youtube" src="<?php echo get_bloginfo('template_url'); ?>/assets/images/header-youtube.png"></a>
                    <a target="_blank" href="<?php echo get_field('instagram', 'option');?>"><img class="instagram" src="<?php echo get_bloginfo('template_url'); ?>/assets/images/header-instagram.png"></a>
                </section>
                <section class="mobile-hide share-btn">
                    <img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/header-shareBtn.png">
                </section>
                <section class="nav-click">
                    <img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/header-menuButton.png">
                    <span class="mobile-hide">Menu</span>
                </section>
            </div>
        </section>
    </header>

    <header class="main-header mobile-show">
        <?php if ( is_front_page() ) { ?>
            <section class="left">
                <a href="/"><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/Logo.png"></a>
            </section>
        <?php } ?>
        <section class="right">
            <div>
                <section class="nav-click">
                    <img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/header-menuButton.png">
                </section>
            </div>
        </section>
    </header>
