<?php
require_once 'lib/rye.php';

/**
 * Set domains for auto tag detection.
 */
define("PRODUCTION_SERVER", "my-production-site.com");
define("STAGING_SERVER", "my-staging-site.com");

/**
 * Set the enviornment property. This will determine which version of the
 * assets will be used.
 */
if (stripos($_SERVER['SERVER_NAME'], PRODUCTION_SERVER) !== -1) {
	Rye::$enviornment = Rye::PRODUCTION;
} else if (stripos($_SERVER['SERVER_NAME'], STAGING_SERVER) !== -1) {
	Rye::$enviornment = Rye::STAGING;
} else {
	Rye::$enviornment = Rye::DEVELOPMENT;
}

/**
 * Site configurations.
 */
Rye::init(array(

	/**
	 *  Path to JavaScript files. Notice that vendor libraries are left separate
	 *  from the custom compiled script. This allows for better compatibility with
	 *  other plugins.
	 *  http://codex.wordpress.org/Function_Reference/wp_register_script
	 */
	'javascripts' => array(
		// 'jquery' => '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js',
		// 'script' => get_bloginfo('template_directory').'/assets/dist/hattaway.all.js',
	),

	/**
	 *  Path to JavaScript files.
	 *  http://codex.wordpress.org/Function_Reference/add_image_size
	 *
	 *  '<image-size-name>' => array(<width>, <height>, <crop>)
	 */
	'image_sizes' => array(
		'story_wall'  => array(380, 320, true),
		/*
		'featured_post'  => array(500, 500, false),
		'featured_article' => array(200, 200, true)
		*/
	),

	/**
	 *  Declare custom menu regions.
	 *  http://codex.wordpress.org/Function_Reference/register_nav_menus
	 */
	'menus' => array(
		/*
		'main-nav' => 'Main Navigation',
		'sub-nav'  => 'Sub Navigation',
		*/
	),

	/**
	 *  Declare theme features.
	 *  http://codex.wordpress.org/Function_Reference/add_theme_support
	 *
	 *  '<feature>' => array('<arg>', '<arg>')
	 */
	'theme_support' => array(
		'post-thumbnails' => array('post'),
		'post-thumbnails' => array('post', 'video_wall')
		/*
		'html5'           => array('search-form', 'comment-form', 'comment-list'),
		'post-thumbnails' => array('post', 'articles'),
		'post-formats'    => array('aside', 'gallery')
		*/
	),

	/**
	 *  Declare "widgetized" regions.
	 *  http://codex.wordpress.org/Function_Reference/register_sidebar
	 */
	'widgetized_regions' => array(
		/*
		array(
			'name'          => '<Region Name>',
			'description'   => '<Region Description>',
			'before_title'  => '<h2>',
			'after_title'   => '</h2>',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		),
		array(
			'name'          => '<Region Name>',
			'description'   => '<Region Description>',
			'before_title'  => '<h2>',
			'after_title'   => '</h2>',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		)
		*/
	),

	/**
	 *  Declare custom post types.
	 *  http://codex.wordpress.org/Function_Reference/register_post_type
	 */
	'post_types' => array(

		'video_wall' => array(
			'labels'      => array('name' => 'Story Wall Videos', 'add_new_item' => 'Add New Story Wall Video', 'edit_item' => 'Edit Story Wall Video', 'new_item' => 'New Story Wall Video', 'view_item' => 'View Story Wall Video', 'search_item' => 'Search Story Wall Videos', 'not_found' => 'No Story Wall Video found'),
			'public'      => true,
			'rewrite'     => true,
			'has_archive' => true,
			'supports'    => array('title', 'thumbnail', 'editor')
		)

	),

	/**
	 *  Declare custom taxonomies.
	 *  http://codex.wordpress.org/Function_Reference/register_taxonomy
	 */
	'taxonomies' => array(
		/*
		array(
			'tax_name', 'postype_name', array(
				'hierarchical' => false,
				'labels'       => array('name' => '<Tax Name>'),
				'show_ui'      => true,
				'query_var'    => true,
				'rewrite'      => array('slug' => 'tax-name'),
			)
		)
		*/
	)
));



/**
 * Prints a menu by given menu name
 */
function printMenu ($menu_name, $link_after = '', $container_class = '', $ul_class = '' ) {
	$defaults = array(
			'theme_location'  => '',
			'menu'            => $menu_name,
			'container'       => 'div',
			'container_class' => $container_class,
			'container_id'    => '',
			'menu_class'      => 'menu ' . sanitize_title($menu_name) . ' ' . $ul_class,
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => $link_after,
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 0,
			'walker'          => ''
	);

	wp_nav_menu( $defaults );
}

// add the options pages
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title'  => 'Options',
		'menu_title'  => 'Options',
		'menu_slug'   => 'options',
		'capability'  => 'edit_posts',
		'redirect'  => false
	));
	acf_add_options_sub_page(array(
		'page_title'  => 'Promo Box',
		'menu_title'  => 'Promo Box',
		'parent_slug' => 'options',
	));
}

/*********************************************************************************************************
//WPML support to page rule with ACF 5
*/
add_filter( 'acf/location/rule_match/page',	'rule_match_post_wpml', 10, 3 );
function rule_match_post_wpml( $match, $rule, $options ) {

	// vars
	$post_id = $options['post_id'];

	// validation
	if( !$post_id ) {
		return false;
	}

	// compare
    if( $rule['operator'] == "==") {

    	$match = ( icl_object_id($options['post_id'],'page',true) == $rule['value'] );

    } elseif( $rule['operator'] == "!=") {

    	$match = ( icl_object_id($options['post_id'],'page',true) != $rule['value'] );

    }

    // return
    return $match;
}

// wpml shortcodes --------------------

add_shortcode( 'wpml_language', 'wpml_find_language');


/*****************************************************************************

 * Shortcode [wpml_language language="en"] [/wpml_language]

 * --------------------------------------------------------------------------- */

function wpml_find_language( $attr, $content = null ){
	extract(shortcode_atts(array(
		'language' => '',
	), $attr));

	return ICL_LANGUAGE_CODE === $language ? $content : '';
}

// Action for registering 'relates' (i.e. 'likes') for story wall
add_action('wp_ajax_user_can_relate', 'user_can_relate');
add_action('wp_ajax_nopriv_user_can_relate', 'user_can_relate');
function user_can_relate() {
	global $wpdb;
	$response = new StdClass;
	$response->success = false;
	if (!isset($_POST['post_id'])) {
		$response->issue = 1;
		die(json_encode($response));
	}
	$post_id = $_POST['post_id'];
	$ip_address = $_SERVER['REMOTE_ADDR'];
	$story_type = $_POST['story_type'];

	$results = user_has_related_to_post($ip_address, $post_id, $story_type);

	if ($results) {
		$response->issue = 2;
		die(json_encode($response));
	}

	// Now we need to insert the relate
	// because the IP address hasn't already related
	$wpdb->insert($wpdb->prefix . 'story_relates', array(
		'ip_address' => $ip_address,
		'post_id' => $post_id,
		'story_type' => $story_type,
	));

	$response->success = true;
	die(json_encode($response));
}

/**
 * Get the 'relates' on story wall items.
 *
 * @param array $ids
 *
 */
function get_story_relates($ids = array()) {
	global $wpdb;
	$sql = 'SELECT post_id,ip_address,story_type FROM ' . $wpdb->prefix . 'story_relates';
	if (sizeof($ids)) {
		$sql .= ' WHERE post_id IN ('.implode(', ', array_fill(0, count($ids), '%s')) . ')';
	}

	// Okay, now we need to compile an array of arguments to send to $wpdb->prepare();
	$args = array_merge(array($sql), $ids);

	$query = !empty($ids) ? call_user_func_array(array($wpdb, 'prepare'), $args) : $sql;
	$results = $wpdb->get_results($query);
	if (empty($results)) {
		return array();
	}

	$stories = array();
	foreach ($results as $result) {
		if (isset($stories[$result->post_id][$result->story_type])) {
			$stories[$result->post_id][$result->story_type]++;
		} else {
			$stories[$result->post_id][$result->story_type] = 1;
		}
	}

	return $stories;
}

function user_has_related_to_post($ip_address, $post_id, $type = '') {
	global $wpdb;
	return (bool) $result = $wpdb->query($wpdb->prepare(
		'SELECT * FROM ' . $wpdb->prefix . 'story_relates
			WHERE ip_address = %s
				AND post_id = %s
				AND story_type = %s' ,
		$ip_address,
		$post_id,
		$type
	));
}

/**
 * ----------------------------------------------------------------------------------------
 * Function to make the theme multiLanguage.
 * ----------------------------------------------------------------------------------------
 */
if ( ! function_exists( 'hattaway_setup' ) ) {
	function hattaway_setup() {
		/**
		 * Make the theme available for translation.
		 */
		$lang_dir = get_template_directory() . '/languages';
		load_theme_textdomain( 'hattaway', $lang_dir );

	}
}

function get_story_similars($tags = array()) {
	if (!empty($tags)) {
		$query = new WP_Query('tag=' . implode(',', $tags));
		if (!empty($query->posts)) {
			return sizeof($query->posts);
		}
	}

	return 0;
}

/*********************************************
* Add new post from form
*********************************************/
/*add_action('wp_print_scripts','include_jquery_form_plugin');
function include_jquery_form_plugin(){
    //if (is_page('12')){ // only add this on the page that allows the upload
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'jquery-form',array('jquery'),false,true );
   // }
}*/


add_action( 'wp_ajax_post_edit_form_tag' , 'post_edit_form_tag' );

function post_edit_form_tag( ) {
   echo ' enctype="multipart/form-data"';
}


//hook the Ajax call
//for logged-in users
add_action('wp_ajax_mq_upload_action', 'mq_ajax_upload');
//for none logged-in users
add_action('wp_ajax_nopriv_mq_upload_action', 'mq_ajax_upload');

add_action("wp_ajax_hfc_addpost", "hfc_addpost");
add_action("wp_ajax_nopriv_hfc_addpost", "hfc_addpost");


function mq_ajax_upload(){
//simple Security check
    //check_ajax_referer('upload_thumb');

	// Data is in $_POST.
//insert data to wordpress
    $array = array(
    	'fromlang',
		'name',
		'city',
		'state',
		'anonymous',
		'hope-tag',
		'hope-text',
		'hope-video',
		'fear-tag',
		'fear-text',
		'fear-video',
		'idea-tag',
		'idea-text',
		'idea-video'
,		'age',
		'gender',
		'sexual_orientation',
		'relationship',
		'income',
		'parental',
		'race',
		'zipcode',
    );

    // Handle checkbox fields that can have multiple values
    $_POST['gender'] = implode(',', $_POST['gender']);
    $_POST['relationship'] = implode(',', $_POST['relationship']);
    $_POST['race'] = implode(',', $_POST['race']);

    $post_id = wp_insert_post( array(
        'post_title'        => 'some title',
        'post_content'      => '',
        'post_status'       => 'publish',
        'post_author'       => '1'
    ) );
    // Leave this
    echo json_encode(array(
    	'url' => get_the_permalink(10), /* 10 is story-wall */
    	'id' => $post_id,
    	'hope-tag' => $_POST['hope-tag'],
    	'fear-tag' => $_POST['fear-tag'],
    	'idea-tag' => $_POST['idea-tag'],
    ));
    foreach ($array as $item) {
		add_post_meta($post_id, $item, isset($_POST[$item]) ? $_POST[$item] : '');
    }

//require the needed files
    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');
//then loop over the files that were sent and store them using  media_handle_upload();
    $attach_id = 0;
    if ($_FILES) {
        foreach ($_FILES as $file => $array) {
            if ($_FILES[$file]['error'] !== UPLOAD_ERR_OK) {
                echo "upload error : " . $_FILES[$file]['error'];
                die();
            }
            $attach_id = media_handle_upload( $file, $post_id );
        }
    }
//and if you want to set that image as Post  then use:
	if ($attach_id) {
		update_post_meta($post_id,'_thumbnail_id',$attach_id);
	}
	die($post_id);
}


function insert_attachment( $file_handler, $post_id, $set_thumb = false ) {
	require_once ABSPATH . 'wp-admin/includes/image.php';
	require_once ABSPATH . 'wp-admin/includes/file.php';
	require_once ABSPATH . 'wp-admin/includes/media.php';

	$attach_id = media_handle_upload( $file_handler, $post_id );

//set post thumbnail (featured)
	if ( $attach_id && $set_thumb ) {
		update_post_meta( $post_id, '_thumbnail_id', $attach_id );
	}

	return $attach_id;
}



// Json dashboard widget

// Function that outputs the contents of the dashboard widget
function dashboard_widget_function( $post, $callback_args ) {
	global $wpdb;

	$tally = $wpdb->get_results("select meta_key, COUNT(*) as count from wp_postmeta where meta_key in ('fear-text', 'hope-text', 'idea-text') AND meta_value <> '' AND meta_value IS NOT NULL GROUP BY meta_key;", ARRAY_A);	

	$total = array_reduce($tally, function($a,$b){return $a += $b['count'];});

	foreach( $tally as $item ){
		echo '<br style="clear:both;" /><span style="font-weight:bold;text-transform:capitalize;">' . explode('-', $item['meta_key'])[0] . '</span><br style="clear:both;" />';
		echo '<div style="position:relative;height:15px;float:left;background:gainsboro;width:' . $item['count'] / $total * 100 . '%;"></div><div style="position:relative;float:left;padding-left:5px;font-size:10px;">' . $item['count'] . '</div>';
	}

	echo "<br style='clear:both;' /><br style='clear:both;' /><a href=\"/download\">Click here to export JSON.</a><br/><a href=\"/download?type=csv\">Click here to export CSV.</a>";


}

// Function used in the action hook
function add_dashboard_widgets() {
	wp_add_dashboard_widget('dashboard_widget', 'Data Snapshot', 'dashboard_widget_function');
}

// Register the new dashboard widget with the 'wp_dashboard_setup' action
add_action('wp_dashboard_setup', 'add_dashboard_widgets' );




/************** zip filter******************/

/*function get_formatted_story_block($id){
		$id = intval($id);
		$story_type = get_post_type($id);
		$post_type_obj = get_post_type_object($story_type);
		$nonce = wp_create_nonce("story_wall_nonce" . $id);


		return array(
			'post_id' => $id,
			);
}

function get_blocks_from_ids($ids) {
	$returnArray = array();
	foreach ($ids as $id) {
		$returnArray[] = get_formatted_story_block($id);
	}
	return $returnArray;
}
*/
add_action( 'init', 'my_custom_init' );
function my_custom_init() {
	remove_post_type_support( 'post', 'editor' );
}


add_action("wp_ajax_kb_search_zip_radius", "kb_search_zip_radius");
add_action("wp_ajax_nopriv_kb_search_zip_radius", "kb_search_zip_radius");
function kb_search_zip_radius() {

	global $wpdb;
	$jsonBlockResults = array();
	$resultZipArray = array();

	// https://zipcodedistanceapi.redline13.com/API
	$api_key = 'QWC9Int7CezLNsJVNEQa5SkhKlGfENXvJzFrW9yyn8kbA36nIXxNPS5AeLPTyNph';
	$jsonURL = 'http://zipcodedistanceapi.redline13.com/rest/' . $api_key . '/radius.json/' . $_REQUEST['zipcode'] . '/25/mile';

	// Initiate curl
	$ch = curl_init();
	// Disable SSL verification
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, $jsonURL);
	$result = curl_exec($ch);
	curl_close($ch);


	$results = json_decode($result);
	if (property_exists($results, 'error_code') || property_exists($results, 'error_msg')) {
		$jsonBlockResults['error'] = true;
		echo json_encode($jsonBlockResults);
		die();
	}
	
	foreach ($results->zip_codes as $result) {
		array_push($resultZipArray, $result->zip_code);
	}


	wp_send_json($resultZipArray);

}



add_action("wp_ajax_story_wall_posts", "story_wall_posts");
add_action("wp_ajax_nopriv_story_wall_posts", "story_wall_posts");

function youtube_id($url){

	$video_id = NULL;
	
	if( strlen( $url ) > 0 ) {
					
		if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
			$video_id = $match[1];
		}
	}

	return $video_id;
}

function story_wall_posts(){
	global $wpdb;
		
	$paged = 1;
	$exclude = array();
	$post_id = NULL;
	$query = NULL;

	if (isset($_REQUEST['paged'])){
		$paged = $_REQUEST['paged'];
	}
	if (isset($_REQUEST['exclude'])){
		$exclude = $_REQUEST['exclude'];
	}
	if (isset($_REQUEST['post_id'])){
		$post_id = $_REQUEST['post_id'];
	}

	$lda_topics = $wpdb->get_results("select meta_value, COUNT(*) from wp_postmeta where meta_key in ('fear_related', 'hope_related', 'idea_related') GROUP BY meta_value ORDER BY CAST(meta_value AS SIGNED);", ARRAY_N);
	if ( $post_id === NULL ){ 
		$query = new WP_Query(array('post_type' => array('post','video_wall'), 'posts_per_page' => 50, 'post__not_in' => $exclude, 'paged' => intval($paged), 'suppress_filters' => 1, 'orderby' => 'date', 'order' => 'DESC'));
	} else {
		$query = new WP_Query(array('post_type' => array('post','video_wall'), 'p' => $post_id, 'posts_per_page' => 50, 'post__not_in' => $exclude, 'paged' => intval($paged), 'suppress_filters' => 1, 'orderby' => 'date', 'order' => 'DESC'));		
	}

	$posts = $query->get_posts();
	$json_array = array();
	$count = 0;
	$post_ids = array();
	$types = array(
		'hope',
		'fear',
		'idea',
	);

	foreach($posts as $post) {
		$language_code = $wpdb->get_row($wpdb->prepare('SELECT language_code FROM wp_icl_translations WHERE element_id = %s', $post->ID));

		if ($post->post_type == 'post') {

			$personal = array(
				'image' => @wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'story_wall')[0]
				,'name' => get_field('name', $post->ID)
				,'city' => get_field('city', $post->ID)
				,'state' => get_field('state', $post->ID)
				,'zipcode' => get_field('zipcode', $post->ID)
			);

			$usedTypes = array();
			foreach ($types as $type) {
				if ($text = get_field("{$type}-text", $post->ID)) {
					$usedTypes[$type] = array(
						'text' => $text,
						'tag' => get_field("{$type}-tag", $post->ID),
						'video' => get_field("{$type}-video", $post->ID),
					);
				}
			}

			foreach ($usedTypes as $type => $usedType) {
				$json_array[$count] = clone $post;

				// This property lets us return the sort to the original order.
				$json_array[$count]->origSort = $count;
				$json_array[$count]->type = $post->post_type;
				$json_array[$count]->topic = $type;
				$json_array[$count]->title = $usedType['tag'];
				$json_array[$count]->title_trim = substr($usedType['tag'], 0, 40);
				$json_array[$count]->text = $usedType['text'];
				$json_array[$count]->text_trim = substr(strip_tags($usedType['text']), 0, 40);
				$json_array[$count]->image = $personal['image'];
				$json_array[$count]->name = $personal['name'];
				$json_array[$count]->city = $personal['city'];
				$json_array[$count]->state = $personal['state'];
				$json_array[$count]->zipcode = $personal['zipcode'];

				// handle video
				$video = $usedType['video'];

				if ( !empty($video) ) {
					$json_array[$count]->video = youtube_id($video);
				} else {
					$json_array[$count]->video = '';
				}
				
				// handle topic modeling info
				$lda = 0;

				$lda_topic = get_field($type . '_related', $post->ID);

				if (is_null($lda_topic) !== true){
					$lda = intval($lda_topics[intval($lda_topic)]['1']) -1 ;
				}

				$json_array[$count]->number_similar = $lda;
				$json_array[$count]->language = $language_code->language_code;
				if ( $count <= 9 ) {
					$json_array[$count]->newest = 'newest';
				} else {
					$json_array[$count]->newest = '';
				}

				//delete if not needed quick fix to have story wall styled
				if ($count % 3 == 0) {
					$json_array[$count]->size = 'large';
				} else {
					$json_array[$count]->size = 'small';
				}

				$count++;
				$post_ids[$post->ID] = false;

			} // end foreach

		} else if ($post->post_type == 'video_wall') {
			$json_array[$count] = clone $post;

			// This property lets us return the sort to the original order.
			$json_array[$count]->origSort = $count;
			$json_array[$count]->type = $post->post_type;
			$json_array[$count]->title = $post->post_title;
			$json_array[$count]->topic = 'video';
			$json_array[$count]->language = $language_code->language_code;
			$video = get_field('video', $post->ID);


			if ( !empty($video) ) {
				$json_array[$count]->video = youtube_id($video);
			} else {
				$json_array[$count]->video = '';
			}
				

			$json_array[$count]->image = @wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'story_wall')[0];
			if ( $count <= 9 ) {
				$json_array[$count]->newest = 'newest';
			} else {
				$json_array[$count]->newest = '';
			}

			//delete if not needed quick fix to have story wall styled
			if ($count % 3 == 0) {
				$json_array[$count]->size = 'large';
			} else {
				$json_array[$count]->size = 'small';
			}

			$count++;
			$post_ids[$post->ID] = false;
		} 
	} 

	$storyRelates = get_story_relates(array_keys($post_ids));

	foreach ($json_array as &$item) {
		if (isset($storyRelates[$item->ID][$item->topic])) {
			$item->relates = $storyRelates[$item->ID][$item->topic];
		} else {
			$item->relates = 0;
		}
	}

	wp_send_json($json_array);

}