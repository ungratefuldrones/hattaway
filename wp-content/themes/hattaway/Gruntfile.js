module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    fileName: '<%= grunt.config.get("pkg").name.toLowerCase().replace(/ /g, "-") %>',
    jsFiles: [
      'assets/js/jquery-1.11.1.min.js',
      'assets/js/jquery.localScroll.js',
      'assets/js/jquery.scrollTo.min.js',
      'assets/js/jquery.panelSnap.js',
      'assets/js/jquery.isotope.min.js',
      'assets/js/jquery.isotope.perfectmasonry.js',
      'assets/js/jquery.selectBox.js',
      'assets/js/jquery.horizontal.scroll.js',
      'assets/js/jquery.waypoints.min.js',
      'assets/js/jquery.cropbox.js',
      'assets/js/jquery.cycle2.min.js',
      'assets/js/jquery.cycle2.carousel.min.js',
      'assets/js/script.js',
    ],
    cssFiles: [
      'assets/css/*.css'
    ],
    lessFiles: [
      'assets/less/styles.less',
    ],
    distDir: 'assets/dist/',
    less: {
      uncompressed: {
        options: {
          compress: false
        },
        files: {
          'assets/css/styles.css': '<%= lessFiles %>'
        }
      }
    },
    concat: {
      options: {
        banner: '/*! <%= fileName %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        stripBanners: true
      },
      js: {
        src: '<%= jsFiles %>',
        dest: '<%= distDir %><%= fileName %>.all.js'
      },
      css: {
        src: '<%= cssFiles %>',
        dest: '<%= distDir %><%= fileName %>.all.css'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= fileName %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: '<%= distDir %><%= fileName  %>.all.js',
        dest: '<%= distDir %><%= fileName %>.all.min.js'
      }
    },
    watch: {
      js: {
        files: '<%= jsFiles %>',
        tasks: ['concat:js']
      },
      less: {
        files: '<%= lessFiles %>',
        tasks: ['less']
      },
      css: {
        files: '<%= cssFiles %>',
        tasks: ['concat:css']
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  return grunt.registerTask('default', ['less', 'concat', 'uglify']);
};
