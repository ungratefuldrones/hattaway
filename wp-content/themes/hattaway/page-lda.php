<?php

	// should save serialized lda object to wp_options to refer to it later, if necessary
	// should probably break into hope fear idea en/es

	if(!isset($_GET['key']) || !in_array($_GET['key'], array('mEcaPaFUbay2waqeBAcheQa8u6ufEw4e'))) wp_die('Unauthorized');

	ini_set('max_execution_time', 1000); 

	$numTopics = 100;

	include('lib/vendor/autoload.php');
 
	use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
	 

	global $wpdb;	

$sql = <<<EOT
SELECT m1.post_id AS 'id', CONCAT( ' ', m1.meta_value, SUBSTRING(m1.meta_key, 1, 4 )) as 'text', SUBSTRING(m1.meta_key, 1, 4) as "type"
FROM wp_postmeta m1 
WHERE 
m1.meta_key IN ('fear-text', 'hope-text', 'idea-text') AND char_length(m1.meta_value)  > 5 
ORDER BY m1.post_id ASC;
EOT;



$fear = $wpdb->get_results($sql, ARRAY_A);

$_stopwords = array('&', 'I','a', '/', 'a', 'w/', '2', 'lgbtq', 'lgbttq', 'lgbt', 'let', 'come', "i'm", 'use', "a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also","although","always","am","among", "amongst", "amoungst", "amount",  "an", "and", "another", "any","anyhow","anyone","anything","anyway", "anywhere", "are", "around", "as",  "at", "back","be","became", "because","become","becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom","but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven","else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own","part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thickv", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves", "the");
$__stopwords = array();

foreach($_stopwords as $w){
	$__stopwords[] = strtolower($w);
}

use NlpTools\FeatureFactories\DataAsFeatures;
use NlpTools\Tokenizers\WhitespaceTokenizer;
use NlpTools\Documents\TokensDocument;
use NlpTools\Documents\TrainingSet;
use NlpTools\Models\Lda;
use NlpTools\Utils\StopWords;
use NlpTools\Stemmers\PorterStemmer;
use NlpTools\Clustering\KMeans;
use NlpTools\Similarity\Euclidean;
use NlpTools\Clustering\CentroidFactories\Euclidean as EuclideanCF;

$stopwords = new StopWords($__stopwords);
$porter = new PorterStemmer();
$tok = new WhitespaceTokenizer();
$tset = new TrainingSet();
foreach ($fear as $f) {

    $tset->addDocument(
        '', // the class is not used by the lda model
        new TokensDocument(
            $tok->tokenize(
                strtolower($f['text'])
            )
        )
    );
}

$tset->applyTransformations(array($stopwords, $porter));

$lda = new Lda(
    new DataAsFeatures(), // a feature factory to transform the document data
    $numTopics, // the number of topics we want
    .01, // the dirichlet prior assumed for the per document topic distribution
    .01  // the dirichlet prior assumed for the per word topic distribution
);
 
// run the sampler 50 times
$lda->train($tset, 75);
 
$wpdb->query("DELETE FROM wp_postmeta where meta_key in ('fear_related', 'hope_related', 'idea_related');");

foreach ( @$lda->wordDocAssignedTopic() as $f=>$t ) {

		$a = array_count_values($t);
		arsort($a);

		if ( current($a) > count($t)/5  ){
			$topic = key($a);

			 $fear[$f] += array('topic' => $topic);
			 update_post_meta($fear[$f]['id'], $fear[$f]['type'] . '_related', $topic);
		}
}

wp_die('Data has been mined.', 'Data mined', array('response' => 200));


