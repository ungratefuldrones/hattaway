<?php
/*
Template Name: Timeline
*/
get_header();

if (have_posts()) {
	while (have_posts()) {
		the_post();

		// Load all timeline sections
		$timeline_sections = get_field('timeline_section');

		// Compile the left-side navigation thingy
		// Array structure should be:
		// array(
		//	'title' => '<title>',
		//	'small_icon' => '<icon_url>',
		//	'large_icon' => '<icon_url>',
		//	'url' => '#<title-slugged>',
		// );

		$timelineNavItems = array();
		foreach ($timeline_sections as $section) {
			$title = $section['timeline_section_title'];

			$small_icon = '';
			if (isset($section['timeline_section_nav_icon_small']['url'])) {
				$small_icon = $section['timeline_section_nav_icon_small']['url'];
			}
			$large_icon = '';
			if (isset($section['timeline_section_nav_icon_large']['url'])) {
				$large_icon = $section['timeline_section_nav_icon_large']['url'];
			}
			$timelineNavItems[] = array(
				'title' => $title,
				'small_icon' => $small_icon,
				'large_icon' => $large_icon,
				'url' => '#' . sanitize_title($title),
			);
		}
		?>

		<section id="page-timeline">
			<div class="page">
				<div class="snap" id="top">
					<div class="container">
						<div class="page-title"><h1><?php the_title(); ?></h1></div>
						<div class="page-content"><?php the_content(); ?></div>
					</div>
				</div>

				<div id="timeline-nav">
					<?php
					/** We should make the line length dynamic based on the number of nav items **/
					$nav_line_background_size = 'background-size: 3px ';
					// Each <li> has a height of 55
					$nav_line_height = (sizeof($timelineNavItems) + 1) * 55;
					// account for some padding just to make sure the line is long enough
					$nav_line_height += 10;

					$nav_line_background_size .= $nav_line_height . 'px;';
					?>
					<ul class="timeline-nav-list" style="<?php echo $nav_line_background_size; ?>">
						<?php
						$first = true;
						foreach($timelineNavItems as $navItem) {
							echo '
							<li>
								<a href="' . $navItem['url'] . '">
									<span class="icon' . ($first ? ' active' : '') . '">
										<img data-icon-small="' . $navItem['small_icon'] . '" data-icon-large="' . $navItem['large_icon'] . '" src="' . ($first ? $navItem['large_icon'] : $navItem['small_icon']) . '" />
									</span>
									<span class="nav-text">'
										. $navItem['title'] .
									'</span>
								</a>
							</li>';
							$first = false;
						}
						?>
					</ul>
					<?php
					// Provide the section IDs to JS for use with Waypoints
					echo '<script>var timelineWaypoints = [];';
					foreach ($timelineNavItems as $navItem) {
						echo 'timelineWaypoints.push("' . $navItem['url'] . '");';
					}
					echo '</script>';
					?>
				</div>

				<div class="snap" id="timeline">
					<div class="timeline-container">
						<h2><?php _e('Growing Up:', 'hattaway'); ?></h2>
						<?php
						foreach ($timeline_sections as $section) {
							$section_title_background_image = $section['timeline_section_title_background'];
							$section_title_background_image_url = isset($section['timeline_section_title_background']['url']) ? $section['timeline_section_title_background']['url'] : '';
							$section_title_background_image_offset_x = isset($section['timeline_section_title_background_offset_x']) ? $section['timeline_section_title_background_offset_x'] : '0px';
							$section_title_background_image_offset_y = isset($section['timeline_section_title_background_offset_y']) ? $section['timeline_section_title_background_offset_y'] : '0px';
							$section_title_inline_style = '';

							if ($section_title_background_image_url) {
								$section_title_inline_style .= "background-image: url('$section_title_background_image_url');";

								if ($section_title_background_image_offset_x || $section_title_background_image_y) {
									$section_title_inline_style .= 'background-position: ';
									$section_title_inline_style .= $section_title_background_image_offset_x;
									$section_title_inline_style .= ' ';
									$section_title_inline_style .= $section_title_background_image_offset_y;
									$section_title_inline_style .= ';';
								}
							}
							echo '<div class="timeline-blocks-container snap" id="' . sanitize_title($section['timeline_section_title']) . '">';
								echo '<h1 style="' . $section_title_inline_style . 'width:' . ($section['timeline_section_title_width'] ? $section['timeline_section_title_width'] : '50') . '%">' . $section['timeline_section_title'] . '</h1>';
								echo '<div class="timeline-blocks">';
								$current_column = 0;
								foreach ($section['timeline_section_blocks'] as $block) {
									$color = $block['timeline_block_color'];
									$background_image = '';
									if (isset($block['timeline_block_background_image']['url'])) {
										$background_image = $block['timeline_block_background_image']['url'];
									}
									$background_color = $block['timeline_block_background_color'];
									$background_transparent = $block['timeline_block_background_transparent'];
									$content = $block['timeline_block_content'];
									$columns = $block['timeline_block_columns'];
									$height = isset($block['timeline_block_height']) ? $block['timeline_block_height'] : '';
									$font_size = $block['timeline_block_font_size'];
									$font_italics = $block['timeline_block_font_italics'];
									$text_align = $block['timeline_block_text_align'];

									$inline_style_string = '';
									if ($background_color && !$background_transparent) {
										$inline_style_string .= 'background-color: ' . $background_color . ';';
									}
									if ($background_image) {
										$inline_style_string .= 'background-image: url(\'' . $background_image . '\');';
										$inline_style_string .= 'background-repeat: no-repeat;';
										$inline_style_string .= 'background-position: ' . $block['timeline_block_background_image_offset_x'] . ' ' . $block['timeline_block_background_image_offset_y'] . ';';
										if (isset($block['timeline_block_background_image_height']) || isset($block['timeline_block_background_image_width'])) {
											$inline_style_string .= 'background-size: ';
											$inline_style_string .= isset($block['timeline_block_background_image_width']) ? $block['timeline_block_background_image_width'] : '100%';
											$inline_style_string .= isset($block['timeline_block_background_image_height']) ? ' ' . $block['timeline_block_background_image_height'] : '';
											$inline_style_string .= ';';
										}

									}
									if ($height) {
										$inline_style_string .= 'height: ' . $height . ';';
									}
									if ($color) {
										$inline_style_string .= 'color: ' . $color . ';';
									}
									if ($font_size) {
										$inline_style_string .= 'font-size: ' . $font_size . 'em;';
									}
									if ($font_italics) {
										$inline_style_string .= 'font-style: italic;';
									}
									if ($text_align) {
										$inline_style_string .= 'text-align: ' . $text_align . ';';
									}
									?>
									<div class="timeline-block<?php echo ' column-' . $columns ?><?php echo $background_transparent ? ' transparent' : ''; ?>" style="<?php echo $inline_style_string; ?>">
										<div class="inner"><?php echo $content; ?></div>
									</div>
									<?php
									$current_column += $columns;

									if ($current_column >= 3) {
										echo '<div style="clear:both"></div>';
										$current_column = 0;
									}
								}
								echo '</div>';
							echo '</div>';
						}
						?>
						<div style="clear:both"></div>
					</div>
				</div>
			</div>
		</section>

		<?php
	}
}

/* Just keeping this just in case. We've already imported the data so hopefully we don't need it anymore.
function hfc_import_data($filename = 'data.txt') {
	ini_set('max_execution_time', 800);
	$array = csvToArray(__DIR__ . "/$filename");
	$iteration = 0;
	$array = array_reverse($array);
	foreach ($array as $newPost) {
		// hard stop at 750
		if ($iteration == 750) {
			break;
		}
		usleep(200000);

		$anonymous = empty($newPost['FIRST NAME']);
		$title = 'Data for ' . ($anonymous ? 'Anonymous' : $newPost['FIRST NAME']);

		$post_id = wp_insert_post(array(
			'post_status'	=> 'publish',
			'post_content'	=> '',
			'post_title'	=> $title
		));

		$keyToKeyAssociation = array(
			'FIRST NAME'					=> 'name',
			'CITY'							=> 'city',
			'STATE'							=> 'state',
			'MY HOPE IS ABOUT'				=> 'hope-tag',
			'MY HOPE FOR OUR TOMORROW IS'	=> 'hope-text',
			'MY FEAR IS ABOUT'				=> 'fear-tag',
			'I WORRY THAT'					=> 'fear-text',
			'MY IDEA IS ABOUT'				=> 'idea-tag',
			'ONE THING OUR MOVEMENT CAN DO'	=> 'idea-text',
			'AGE'							=> 'age',
			'RACE/ETHNICITY'				=> 'race',
			'SEXUAL ORIENTATION'			=> 'sexual_orientation',
			'GENDER IDENTITY'				=> 'relationship',
			'INCOME LEVEL'					=> 'income',
			'PARENTAL STATUS'				=> 'parental',
			'ZIP CODE'						=> 'zipcode',
		);

		foreach ($keyToKeyAssociation as $from => $to) {
			if (!empty($newPost[$from])) {
				add_post_meta($post_id, $to, $newPost[$from]);
			}
		}

		if ($anonymous) {
			add_post_meta($post_id, 'anonymous', true);
		}

		add_post_meta($post_id, 'language', 'en');
		error_log("added post $post_id");

		// wp_insert_post(array(
		// 	'post_id' => $post_id,
		// 	'post_name' => $post_id,
		// ));

		$iteration++;
	}
}
// Function to convert CSV into associative array
function _csvToArray($file, $delimiter = '	') {
	if (($handle = fopen($file, 'r')) !== FALSE) {
		$i = 0;
		while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) {
			for ($j = 0; $j < count($lineArray); $j++) {
				$arr[$i][$j] = $lineArray[$j];
			}
			$i++;
		}
		fclose($handle);
	}
	return $arr;
}

function csvToArray($file, $delimiter = '	') {
	$keys = $newArray = array();

	$data = _csvToArray($file, $delimiter);
	// Set number of elements (minus 1 because we shift off the first row)
	$count = count($data) - 1;

	//Use first row for names
	$labels = array_shift($data);

	// // Add Ids, just in case we want them later
	// $labels[] = 'ID';

	// for ($i = 0; $i < $count; $i++) {
	// 	$data[$i][] = $i;
	// }

	// Bring it all together
	for ($j = 0; $j < $count; $j++) {
		$d = array_combine($labels, $data[$j]);
		$newArray[$j] = $d;
	}

	return $newArray;
}*/

get_footer();