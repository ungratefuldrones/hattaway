<?php
/*
Template Name: Story Wall
*/
get_header(); ?>

<?php if ( have_posts() ) :?>
<?php while ( have_posts() ) : the_post(); ?>

	<a href="/" class="top-title mobile-show"><?php _e('Our Tomorrow', 'hattaway'); ?></a>
	<div class="top-gradient"></div>


<?php
	$lda_topics = $wpdb->get_results("select meta_value, COUNT(*) from wp_postmeta where meta_key in ('fear_related', 'hope_related', 'idea_related') GROUP BY meta_value ORDER BY CAST(meta_value AS SIGNED);", ARRAY_N);
	$query = new WP_Query(array('post_type' => array('post','video_wall'), 'posts_per_page' => -1, 'suppress_filters' => 1, 'orderby' => 'date', 'order' => 'DESC'));
	$posts = $query->get_posts();
	$json_array = array();
	$count = 0;
	$post_ids = array();

	foreach($posts as $post) {
		$language_code = $wpdb->get_row($wpdb->prepare('SELECT language_code FROM wp_icl_translations WHERE element_id = %s', $post->ID));

		if ($post->post_type == 'post') {
		$types = array(
			'hope',
			'fear',
			'idea',
		);
		$usedTypes = array();
		foreach ($types as $type) {
			if ($text = get_field("{$type}-text", $post->ID)) {
				$usedTypes[$type] = array(
					'text' => $text,
					'tag' => get_field("{$type}-tag", $post->ID),
						'video' => get_field("{$type}-video", $post->ID),
				);
			}
		}

		foreach ($usedTypes as $type => $usedType) {
			$json_array[$count] = $post;

			// This property lets us return the sort to the original order.
			$json_array[$count]->origSort = $count;
			$json_array[$count]->type = $post->post_type;
			$json_array[$count]->topic = $type;
			$json_array[$count]->title = $usedType['tag'];
			$json_array[$count]->title_trim = substr($usedType['tag'], 0, 40);
			$json_array[$count]->text = $usedType['text'];
			$json_array[$count]->text_trim = substr(strip_tags($usedType['text']), 0, 40);


				$video = $usedType['video'];

				if ( !empty($video) ) {
					$video_parts = explode('v=', $video);
				$json_array[$count]->video = $video_parts[1];
			}
			else {
				$json_array[$count]->video = '';
			}
			$lda = 0;

			$lda_topic = get_field($type . '_related', $post->ID);

			if (is_null($lda_topic) !== true){
				$lda = intval($lda_topics[intval($lda_topic)]['1']) -1 ;
			}
			$json_array[$count]->image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'story_wall')[0];
			$json_array[$count]->name = get_field('name', $post->ID);
			$json_array[$count]->city = get_field('city', $post->ID);
			$json_array[$count]->state = get_field('state', $post->ID);
			$json_array[$count]->zipcode = get_field('zipcode', $post->ID);
			$json_array[$count]->number_similar = $lda;
			$json_array[$count]->language = $language_code->language_code;
				if ( $count <= 9 ) {
					$json_array[$count]->newest = 'newest';
				} else {
					$json_array[$count]->newest = '';
				}

				//delete if not needed quick fix to have story wall styled
				if ($count % 3 == 0) {
					$json_array[$count]->size = 'large';
				} else {
					$json_array[$count]->size = 'small';
				}

				$count++;
				$post_ids[$post->ID] = false;
			}
		} else if ($post->post_type == 'video_wall') {
			$json_array[$count] = $post;

			// This property lets us return the sort to the original order.
			$json_array[$count]->origSort = $count;
			$json_array[$count]->type = $post->post_type;
			$json_array[$count]->title = $post->post_title;
			$json_array[$count]->topic = 'video';
			$json_array[$count]->name = get_field('name', $post->ID);
			$json_array[$count]->city = get_field('city', $post->ID);
			$json_array[$count]->state = get_field('state', $post->ID);
			$json_array[$count]->language = $language_code->language_code;
			$video = get_field('video', $post->ID);

			if ( !empty($video) ) {
				$video_parts = explode('v=', $video);
				$json_array[$count]->video = $video_parts[1];
			} else {
				$json_array[$count]->video = '';
			}

			$json_array[$count]->image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'story_wall')[0];
			if ( $count <= 9 ) {
				$json_array[$count]->newest = 'newest';
			} else {
				$json_array[$count]->newest = '';
			}

			//delete if not needed quick fix to have story wall styled
			if ($count % 3 == 0) {
				$json_array[$count]->size = 'large';
			} else {
				$json_array[$count]->size = 'small';
			}

			$count++;
			$post_ids[$post->ID] = false;
		}
	}

	$storyRelates = get_story_relates(array_keys($post_ids));

	foreach ($json_array as &$item) {
		if (isset($storyRelates[$item->ID][$item->topic])) {
			$item->relates = $storyRelates[$item->ID][$item->topic];
		} else {
			$item->relates = 0;
		}
	}




?>

<script>
	var storyitems = <?php echo json_encode($json_array); ?>;
</script>

<section class="story-wall first">
	<div class="container-alt2">
		<section class="filters">
			<div class="sort">Sort</div>
			<ul>
				<li class="active"><a href="" data-filter="*"><?php _e('All', 'hattaway'); ?></a></li>
				<li><a href="" data-sort-by="relates"><?php _e('Trending', 'hattaway'); ?></a></li>
				<li><a href="" data-filter="newest"><?php _e('Newest', 'hattaway'); ?></a></li>
				<li>
					<a class="blank" href=""><?php _e('Topic', 'hattaway'); ?></a>
					<ul>
						<li class="sub"><a href="" data-filter="hope"><?php _e('Hope', 'hattaway'); ?></a></li>
						<li class="sub"><a href="" data-filter="fear"><?php _e('Fear', 'hattaway'); ?></a></li>
						<li class="sub"><a href="" data-filter="idea"><?php _e('Idea', 'hattaway'); ?></a></li>
					</ul>
				</li>
				<li class="radius-search">
					<a href="" ><?php _e('Nearby', 'hattaway'); ?></a>
					<ul>
						<li>
							<form id='story-wall-radius-search' action="" method="post">
								<input class="search-for" placeholder="<?php _e('Enter Zip Code', 'hattaway'); ?>" type="text" />
								<div class="submit"></div>
							</form>
						</li>
					</ul>
				</li>
				<li>
					<a class="blank" href=""><?php _e('State', 'hattaway'); ?></a>
					<ul>
						<?php
						foreach (json_decode(file_get_contents(get_template_directory() . '/assets/js/states.json')) as $stateAbbrev => $stateName) {
							echo '<li class="sub"><a href="" data-filter="' . $stateAbbrev . '">' . $stateName . '</a></li>';
						}
						?>
					</ul>
				</li>
				<li class=""><a href="" data-filter="video">Video</a></li>
				<li>
					<a class="blank" href=""><?php _e('Language', 'hattaway'); ?></a>
					<ul>
						<li class="sub"><a href="" data-filter="en"><?php _e('English', 'hattaway'); ?></a></li>
						<li class="sub"><a href="" data-filter="es"><?php _e('Spanish', 'hattaway'); ?></a></li>
					</ul>
				</li>
			</ul>
		</section>

		<section class="story-wall-isotope"></section>

		<div id="more-button">
			<button class="load-more"><?php _e('Load More', 'hattaway'); ?></button>
		</div>
	</div>

	<section class="overlay story-overlay"></section>
</section>



<script id="storyItemTemplate" type="text/template">

	<div class="inner" data-newest="<%= newest %>" data-topic="<%= topic %>" data-state="<%= state %>" data-size="<%= size %>" data-id="<%= ID %>" data-relates="<%= relates %>" data-language="<%= language %>" data-orig-sort="<%= origSort %>" data-video="<% if (video) {%><%= video %><%}%>" data-specialVideo="<% if (type == 'video_wall') {%>video<%}%>" data-zipcode="<% if ((type == 'post') && (zipcode)) {%><%= zipcode %><%}%>" <% if (image) { %>style="background-image: url('<%= image %>');"<% } %>>
		<% if ( type == 'video_wall' ) { %>
			<div class="special-video-color-overlay"></div>
			<div class="special-video copy">
				<h2 class="topic"><a href=""><%= title %></a></h2>
				<div class="play-btn"></div>
			</div>
		<% } else { %>
			<div class="color-overlay"></div>
			<div class="copy">
				<h2 class="topic"><a href=""><span><%= topic %></span>
					<% if (title) { %> <%= title_trim %><% } else { %> <%= text_trim %>...<% } %>
				</a></h2>
				<% if (size === 'large') { %>
					<p><% if (name) { %>- <%= name %>, <%= city %>, <%= state %><% } else { %>- <?php _e('Anonymous', 'hattaway') ?> <% } %></p>
					<div class="line"></div>
						<h3><span class="relates" id="story<%= ID %>relates"><%= relates %></span> <span class="people-person"><% if (relates == 1) { %><?php _e('person', 'hattaway'); ?><% } else { %><?php _e('people', 'hattaway'); ?><% } %></span> <span><?php _e('can relate', 'hattaway'); ?></span><br/><span class="shares" id="story<%= ID %>shares">0</span> <span><?php _e('people shared', 'hattaway'); ?></span><br/><span class="similars"></span><%=  number_similar %> <span><?php _e('people have similar', 'hattaway'); ?> <%= topic %>s</span></h3>
					</h3>
					<div class="links">
						<a href="" class="relate" data-id="<%= ID %>" data-topic="<%= topic %>"><?php _e('I can relate', 'hattaway'); ?></a>
							<a href="" class="share" data-url="<?php echo get_the_permalink(10); /* 10 is the story-wall page id */ ?>#<%= topic %>/<%= ID %>" data-video="<% video %>" data-image="<%= image %>" data-id="<%= ID %>" data-title="<% if (title) { %> <%= title %> <% } else { %> <%= text %> <% } %>"><?php _e('Share', 'hattaway'); ?></a>
					</div>
				<% } %>
				<% if (video) { %>
					<div class="play-btn"></div>
				<% } %>
			</div>
		<% } %>
	</div>
</script>

<script id="storyOverlayTemplate" type="text/template">
 	<a class="close" href=""><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/CloseBtn_Small.png"></a>
    <div class="pager">
		<a href="" class="next"><?php _e('Next', 'hattaway'); ?></a>
		<a href="" class="prev"><?php _e('Prev', 'hattaway'); ?></a>
	</div>
</script>

<script id="storyInnerOverlayTemplate" type="text/template">

    <div class="story-inner" data-topic="<%= topic %>" data-id="<%= ID %>" data-url="<?php echo get_the_permalink(10); /* 10 is the story-wall page id */ ?>#<%= topic %>/<%= ID %>" <% if (image) { %>style="background-image: url('<%= image %>');"<% } %>>

    	<% if ( type === 'video_wall' ) { %>
    		<div class="special-video-color-overlay"></div>
			<div class="special-video copy">
				<h2><%= title %></h2>

	            <iframe width="100%" height="290" src="https://www.youtube.com/embed/<%= video %>?showinfo=0" frameborder="0"></iframe>
			</div>
    	<% } else { %>
	        <div class="color-overlay"></div>
	        <div class="copy">
	        <?php if (is_user_logged_in()): echo '<a href="/wp-admin/post.php?post=<%= ID %>&action=edit">Moderate</a>'; endif; ?>
	            <h2><span><%= topic %></span> <% if (title) { %> <%= title %><% } else { %> <%= text %><% } %></h2>
	            <% if (video != '') { %>
	            	<iframe width="100%" height="290" src="https://www.youtube.com/embed/<%= video %>?showinfo=0" frameborder="0"></iframe>
	            <% } %>
	            <% if (title) { %> <p><%= text %></p><% } %>
	            <p><% if (name) { %>- <%= name %>, <%= city %>, <%= state %><% } else { %>- <?php _e('Anonymous', 'hattaway') ?> <% } %></p>
	            <div class="line"></div>
		            <h3><span class="relates" id="story<%= ID %>relates"><%= relates %></span> <span class="people-person"><% if (relates == 1) { %><?php  _e('person', 'hattaway'); ?><% } else { %><?php  _e('people', 'hattaway'); ?><% } %></span> <span><?php _e('can relate', 'hattaway'); ?></span><br/><span class="shares" id="story<%= ID %>shares">0</span> <span><?php _e('people shared', 'hattaway'); ?></span><br/><%=  number_similar %> <span><?php _e('people have similar', 'hattaway'); ?> <%= topic %>s</span></h3>
	            </h3>
	        </div>
	        <div class="links">
					<a href="" class="relate" data-id="<%= ID %>" data-topic="<%= topic %>"><?php _e('I can relate', 'hattaway'); ?></a>
					<a href="" class="share" data-url="<?php echo get_the_permalink(10); /* 10 is the story-wall page id */ ?>#<%= topic %>/<%= ID %>" data-title="<%= title_trim %>"><?php _e('Share', 'hattaway'); ?></a>

			</div>
		<% } %>

    </div>

</script>

<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>

<div class="bottom-sticky">
	<a href="" class="fl-left watch video home-video-click"><?php _e('Watch the Video', 'hattaway'); ?></a>
	<a href="" class="fl-right plus open-form-overlay"><span><?php _e('Share Your Voice', 'hattaway'); ?></span><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/icon-OrangePlusIcon_Small.png"></a>
</div>

<?php get_footer (); ?>
