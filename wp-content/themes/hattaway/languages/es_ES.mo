��    \      �     �      �     �     �     �  B   �     ,  %   4     Z     c     }     �     �     �     �     �  
   �     �     �      �     �     		     	     	     8	     ?	     O	     T	     a	     j	     �	     �	  	   �	     �	  (   �	     �	     �	     
     
     #
  )   1
     [
     b
     i
     n
  �   z
            A   <     ~     �  Y   �  <   �  	   -     7  r   G     �     �     �     �     �     �                    ,     B     I     O  	   X  V   b     �     �     �  
   �     �     �  	                  2     H     N  #   V     z  O   �  a   �  
   @  	   K     U     o  E   }  ;   �  �  �     �     �     �  G   �     �  +   �          &  
   <     G     N  *   \     �     �     �  
   �     �  !   �  
   �  	   �     �  #        0     7     K     P     b     n     �     �     �  	   �  /   �     �  )         *  
   >     I  .   X     �  
   �  	   �     �  �   �  $   G  $   l  [   �     �     �  d     E   i  	   �     �  �   �     Y     e     n     w     �     �     �  	   �     �     �  
   �                 Z        u     z  	   �     �     �  
   �     �     �     �     �            1   %     W  `   g  d   �     -     @      E     f  ^   |  O   �     Q   S   )   &      -   [              L   P             X           A   ?          $   Z                 4   R   :   M       <   +   1      W   V   I      !   O                     /   Y   H   .       E       D                    0      '   7   6          >   G                     F   	              \       
               =      #       ,           @   (              C         %   U   5      ;         8   *   B                  N   T       "   9   3       J   2   K       Age Agender All American Indian, Native/Indigenous American or Alaska Native Asian Asexual Asian, South Asian or Southeast Asian Bisexual Black or African-American By State City Divorced Enter Youtube video id here. Fear Female First Name Gay Gender Identity Gender Nonconforming/Genderqueer Growing Up: Hope I can relate I choose to remain anonymous I hope I worry that... Idea Income Level Intersex Join the Conversation Latino or Spanish Origins Lesbian Load More Male Married/Civil Union/Domestic Partnership My fear is... My hope for our tomorrow is... My hope is... My idea is to My idea is... Native Hawaiian or Other Pacific Islander Nearby Newest Next No Children On the following screens, you can share your hopes and fears for the future—and your ideas for making tomorrow better for all of us. One or More Children Over 18 One or More Children Under 18 One thing our movement can do to make our tomorrow brighter is... Open Relationship Other Our Tomorrow is a campaign by more than 70 LGBTQ nonprofit organizations and foundations  Our Tomorrow will keep your answers completely confidential. Pansexual Parental Status Please help us understand the issues that matter most to you and others like you by answering the questions below. Polyamorous Prev Queer Questioning Race / Ethnicity Relationship Status Sexual Orientation Share Share Your Voice Share on Social Media Single State Straight Thank You Thanks for sharing your voice! Now, share it with your friends using the button below. Topic Transgender Trending Two-Spirit Unmarried Partnership Upload Photo Use Video Watch the Video What is Our Tomorrow? What is our tomorrow? White Widowed You may choose to remain anonymous. Your Voice Matters: Your ideas will help build a bigger, bolder movement that leaves no one behind. Your personal information will never be sold or shared — and it will not appear with your post. can relate hey there people have similar hopes people shared that we will soon be able to enjoy the healthcare options we deserve. to engage our community in a conversation about our future. Project-Id-Version: Hattaway
POT-Creation-Date: 2015-04-10 14:24-0500
PO-Revision-Date: 2015-04-10 14:26-0500
Last-Translator: 
Language-Team: Home Front <quintero@homefront.tv>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.5
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Edad Agender Todos Indios americanos, indígenas americanos o asiáticos nativos de Alaska Asexual Asiático, Asia del sur o sureste asiático Bisexual Negro o Afroamericano Por Estado Ciudad Divorciado(a) Introduzca el id del vídeo Youtube aquí. Temor Femenino Primer Nombre Homosexual Identidad de Género Género No Conforme / Genderqueer Creciendo: Esperanza Puedo relacionar Yo elijo permanecer en el anonimato Espero Me preocupa que ... Idea Nivel de ingresos Intersexual Únase a la conversación Origen español o Latino Lesbiana Cargar más Masculino Casado / Unión Civil  / Asociación Doméstica Mi temor es ... Mi esperanza para nuestro  mañana es ... Mi esperanza es ... Mi idea es Mi idea es ... Nativo hawaiano u otros isleños del Pacífico Cercano Más nuevo Siguiente Sin Hijo(s) En las siguientes pantallas, usted puede compartir sus esperanzas y miedos de cara al futuro y sus ideas para hacer mañana mejor para todos nosotros. Uno o más hijos mayores de 18 años Uno o más hijos menores de 18 años Es una cosa que puede hacer nuestro movimiento para hacer nuestra mañana más brillante... Relación abierta Otro Nuestro mañana es una campaña por más de 70 organizaciones sin fines de lucro y fundaciones LGBTQ Nuestro Mañana mantendrá sus respuestas completamente confidencial. Pansexual Estatus paterno Por favor, ayudanos a comprender los problemas o temas que más le importan a usted y ha otros como usted contestando las siguientes preguntas. Polyamorous Anterior Maricón Cuestionamiento Raza / Etnicidad Estado de relación Orientación sexual Compartir Comparte Tu Voz Compartir en las Redes Sociales Soltero(a) Estado Recto Gracias Gracias por compartir tu voz! Ahora, compártelo con tus amigos usando el botón de abajo. Tema Transgénero Tendencia Dos-espíritus Asociación soltero Subir Foto Usar el vídeo Mira el Vídeo ¿Cuál es Nuestro Mañana? ¿Cuál es nuestro mañana? Blanco Viudo(a) Usted puede optar por permanecer en el anonimato. Tu voz importa: Sus ideas ayudará a construir un movimiento más grande, más audaz que no deja a nadie atrás. Su información personal nunca será vendida o compartida - y no va a aparecer con su intervención. puede relacionarse Hola gente tiene esperanzas similares personas compartieron que pronto seremos capaces de disfrutar las opciones de cuidado de la salud que nos merecemos. para participar en nuestra comunidad en una conversación sobre nuestro futuro. 